<?php
include('class/auth.php');
if (isset($_GET['user_id'])) {
    if ($_GET['user_id'] == $input_by) {
        $new_user_id = $input_by;
    } else {
        header('location: user-about-me.php?user_id=' . $_GET['user_id']);
        exit();
    }
} else {
    $new_user_id = $input_by;
}
$new_page_id = $_GET['page_id'];
//profile extra heafer file and script script
include('plugin/profile_extra_headfile.php');
//profile extra heafer file and script script
//chat box script
include('plugin/chat_box.php');
//chat box script 
?>

<?php
//chat user list
include('plugin/chat_box_head_list.php');
//chat user list 
?>
<div class="main-container page-container section-padd">
    <div class="container">
        <div class="row">
            <?php
//profile photo and cover photo
            //include('plugin/profile_photo_n_cover.php');
//profile photo and cover photo
            ?>
        </div>

        <div style="clear: both"></div>

        <div class="row profile-content-row">
            <div class="col-md-9" style="padding-left: 0px;">
                <div class="panel ">
                    <div class="panel-heading">
                        <h4 class="pull-left"> <i class="glyphicon glyphicon-user"></i> Page settings </h4>


                        <div class="panel-tools pull-right">


                            <ul class="panel-actions actions" style="margin-left: ">


                                <li>
                                    <a href="setting.php">
                                        <i class="mdi-editor-mode-edit"></i> Edit Profile Info
                                    </a>
                                </li>
                            </ul>





                        </div>    <div style="clear: both"></div>
                    </div>
                    <div class="panel-body">
                        <script>
                            $(document).ready(function () {
                                $("#overview-btn").click(function () {
                                    $("#overview-panel").show('slow');
                                });
//                                
                            });</script>


                        <div id="photo-content" style="clear:both;" class="row">
                            <div class="col-md-4">
                                <div class="panel panel-default">
                                    <div class="panel-body p-0">
                                        <div class="list-group text-left">

                                            <a style="padding-top: 5px !important; padding-bottom: 5px !important;" id="overview-btn" href="page_about.php?page_id=<?php echo $_GET['page_id']; ?>" class="list-group-item btn btn-block btn-success padding-tb10"><i class="mdi-action-settings-applications">&nbsp;&nbsp;</i>About Page Setting </a>
                                            <a style="padding-top: 5px !important; padding-bottom: 5px !important;" id="milestons-btn" href="page_settings.php?page_id=<?php echo $_GET['page_id']; ?>"  class="list-group-item btn btn-block btn-success padding-tb10"><i class="fa fa-graduation-cap">&nbsp;&nbsp;</i> General Setting</a>
                                            <a style="padding-top: 5px !important; padding-bottom: 5px !important;" id="pgowners-btn" href="page_roles.php?page_id=<?php echo $_GET['page_id']; ?>" class="list-group-item btn btn-block btn-success padding-tb10"><i class="fa fa-map-marker">&nbsp;&nbsp;</i>Page Roles</a> 
                                            <a style="padding-top: 5px !important; padding-bottom: 5px !important;"  id="pgowners-btn" href="page_moderation.php?page_id=<?php echo $_GET['page_id']; ?>" class="list-group-item btn btn-block btn-success padding-tb10"><i class="fa fa-map-marker">&nbsp;&nbsp;</i>Page Moderation</a> 



<!--                                            <a href="#" class="list-group-item no-border"><i class="mdi-social-notifications"></i>Life Events</a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8" style="border-left: 1px solid #ccc;">


                                <fieldset id="profile-overview-1"><!--field set for profile overview starts here-->
                                    <h5 class="bold" style="border-bottom: 1px dashed #ccc; padding-bottom: 10px;"><i class="fa fa-info-circle">&nbsp;&nbsp;</i>Profile Overview&nbsp;:&nbsp;All Basic Page Information</h5>

                                    <div id="all_everyone">
                                        <div class="last"> <!--Favourites start-->

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <div class="col-md-12"  id="category_from" style=" background:#F5F5F5; border: 1px #0cc solid; padding:15px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Favourites</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 " id="category_panel">
                                                                    <?php
                                                                    $fav = $obj->SelectAllByVal("dostums_fanpage", "page_id", $new_page_id, "favourite");
                                                                    ?>
                                                                    <div class="col-md-10" style="padding-left:14px;">
                                                                        <?php echo $fav; ?>
                                                                    </div>


                                                                    <div class="col-md-2">    
                                                                        <span style="color: #2C99CE; cursor: pointer;"> Edit</span><br>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--Favourites end-->

                                            <div class="col-md-12" id="category_from_panel" style="margin-bottom: 20px; display: none; border: 1px #69BD45 solid;background-color:#E6E6E6; padding: 10px 0;"> <!--Favourites hidden start-->

                                                <div class="col-md-3">
                                                    <label class="control-label" for="textinput">
                                                        <strong  style="padding-left:16px;">Favourites</strong>
                                                    </label>
                                                </div>
                                                <div class="col-md-8" style="padding-left:63px;">

                                                    <div class="checkbox" style="">
                                                        <label>
                                                            <input type="checkbox" id="checkfav" style="margin-top:20px;"> Add Page to your favourites[?]
                                                        </label>
                                                    </div>



                                                </div>
                                                <div class="col-md-12" style="padding-left:198px;">
                                                    <button id="category_btn" type="submit" class="btn btn-success btn-sm">Save Change</button>
                                                    <button type="submit" class="btn btn-danger btn-sm" id="category_close">cancel</button>

                                                </div>
                                            </div><!--Favourites hidden end-->

                                        </div>
                                    </div><!--Favourites 2end-->
                                    <script>

                                    </script>

                                    <style type="text/css">
                                        #category_close,#category_from{ cursor: pointer; }
                                        #category_close:hover{ cursor: pointer; color: #000; }
                                    </style>
                                    <script>
                                        $('document').ready(function () {
                                            $('#category_from, #category_close').click(function () {
                                                $('#category_from_panel').toggle('slow');
                                            });
                                            $("#category_btn").click(function () {
                                                var page_id = '<?php echo $new_page_id; ?>';

                                                var favyesno = 'No';
                                                if (document.getElementById('checkfav').checked == true)
                                                {
                                                    var favyesno = 'Yes';
                                                }
                                                else
                                                {
                                                    var favyesno = 'No';
                                                }

                                                //console.log(favyesno);

                                                $.post("./lib/fanpage.php", {'st': 16, 'fav': favyesno, 'page_id': page_id}, function (data) {
                                                    if (data == 1)
                                                    {
                                                        $('#category_panel').html(favyesno);
                                                        $('#category_from_panel').toggle('slow');
                                                        //alert('Congrats!!! Successful.');
                                                    }
                                                    else
                                                    {
                                                        alert('Sorry!!! Failed. Please Try Again.');
                                                    }
                                                });
                                            });
                                        });</script>








                                    <div class="row form-group"> <!--Page visibility start-->
                                        <div class="col-sm-12">
                                            <div class="col-md-12"  style=" background:#F5F5F5; border: 1px #0cc solid; padding: 5px;">
                                                <div class="control-group" id="fanpage_from" style="margin-top:10px; padding-left:11px;">
                                                    <div class="col-md-3">
                                                        <label class="control-label" for="textinput"><strong> Page visibility</strong></label>
                                                    </div>
                                                    <div class="col-md-9">

                                                        <div class="col-md-12 ">
                                                            <div class="col-md-9" id="name_place" style="padding-left:10px;" >

                                                                <?php
                                                                $pagename = $obj->SelectAllByVal("dostums_fanpage", "page_id", $new_page_id, "name");
                                                                echo $pagename;
                                                                ?>  
                                                            </div>
                                                            <div class="col-md-2">    
                                                                <span style="color: #2C99CE; padding-left:20px; cursor: pointer;"> Edit</span><br>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!--Page visibility end -->




                                    <div class="col-md-12" id="fanpage_name" style="margin-bottom: 20px; display: none; border: 1px #69BD45 solid;background-color:#E6E6E6; padding: 10px 0;"> <!--Page visibility hidden start-->

                                        <div class="col-md-4">
                                            <label class="control-label" for="textinput">
                                                <strong  style="padding-left:17px;">Page visibility</strong>
                                            </label>
                                        </div>
                                        <div class="col-md-8" style="padding-left:0px;" >
                                            <div class="col-md-11 controls">

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" style="margin-top:20px;">&nbsp;Unpublish Page[?]
                                                    </label>
                                                </div>


<!--                                                <input type="text" id="fanpage_fill" class="form-control" value="<?php // echo $pagename;                   ?>"  >-->
                                            </div>
                                            <!--                                        <div class="col-md-1">  
                                                                                        <span class="text-right" style="position: absolute;" id="dostums_close"><i class="fa fa-close"></i></span>
                                                                                    </div>-->
                                            <div class="col-md-12"style="padding-left:16px;">
                                                <button type="submit" id="fan_save" class="btn btn-success btn-sm">Save change</button>
                                                <button type="submit" id="Posts_panel_close" class="btn btn-danger btn-sm">cancel</button>

                                            </div>



                                        </div>

                                    </div><!--Page visibility hidden end-->




                                    <style>
                                        #web_dostums_from,#Posts_panel_close{cursor:pointer;}
                                        #Posts_panel_close:hover{cursor:pointer; color:#000;}
                                    </style>

                                    <script>
                                        $('document').ready(function () {
                                            $('#fanpage_from, #Posts_panel_close').click(function () {
                                                $('#fanpage_name').toggle('slow');
                                            });
                                            $("#fan_save").click(function () {
                                                var fanpage_fill = $('#fanpage_fill').val();
                                                var page_id = '<?php echo $new_page_id; ?>'
                                                $.post("./lib/fanpage.php", {'st': 4, 'fanpage_fill': fanpage_fill, 'page_id': page_id}, function (data) {
                                                    if (data == 1)
                                                    {
                                                        $('#name_place').html(fanpage_fill);
                                                        alert('Congrats!!! Successful.');
                                                        $('#fanpage_name').toggle('slow');
                                                    }
                                                    else
                                                    {
                                                        alert('Sorry!!! Failed. Please Try Again.');
                                                    }
                                                });
                                            });
                                        });</script> 










                                    <div class="last"> <!--Messages start-->

                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="col-md-12"  id="web_dostums_from" style=" background:#F5F5F5; border: 1px #0cc solid; padding:10px;">
                                                    <div class="control-group">
                                                        <div class="col-md-4">
                                                            <label class="control-label" for="textinput" style="padding-left:6px;"><strong>Messages</strong></label>
                                                        </div>
                                                        <div class="col-md-8">

                                                            <div class="col-md-12 " >
                                                                <div class="col-md-10"  id="web_panel" style="padding-left:0px;" >
                                                                    <?php
                                                                    $webpage = $obj->SelectAllByVal("dostums_fanpage", "page_id", $new_page_id, "dostums_web_address");
                                                                    echo $webpage;
                                                                    ?>
                                                                </div>

                                                                <div class="col-md-2" style="padding-left:6px;">    
                                                                    <span style="color: #2C99CE;"> Edit</span><br>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--Messages end-->

                                    <div class="col-md-12" id="dostums_panel" style="margin-bottom: 20px; display: none; border: 1px #69BD45 solid;background-color:#E6E6E6; padding: 10px 0;"> <!--Messages hidden start-->

                                        <div class="col-md-3">
                                            <label class="control-label" for="textinput">
                                                <strong  style="padding-left:18px;">Messages</strong>
                                            </label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="col-md-11 controls" style="padding-left:40px;">

                                                <div class="checkbox" style="">
                                                    <label>
                                                        <input type="checkbox" style="margin-top:20px;">&nbsp;Allow people to contact my Page privately by showing the Message button
                                                    </label>
                                                </div>


<!--                                                <input type="text" id="web_address"  class="form-control" value="<?php //echo $webpage;                 ?>" >-->
                                            </div>
                                            <!--                                        <div class="col-md-1">    
                                                                                        <span class="text-right" style="position: absolute;" id="dostums_close"><i class="fa fa-close"></i></span>
                                                                                    </div>-->
                                            <div class="col-md-12" style="padding-left:40px;">
                                                <button type="submit" class="btn btn-success btn-sm" id="web_save">Save change</button>
                                                <button type="submit" class="btn btn-danger btn-sm" id="web_close">cancel</button>

                                            </div>



                                        </div>

                                    </div><!--Messages end-->




                                    <style>
                                        #web_dostums_from,#web_close{cursor:pointer;}
                                        #web_close:hover{cursor:pointer;color:#000;}
                                    </style>

                                    <script>
                                        $('document').ready(function () {
                                            $('#web_dostums_from, #web_close').click(function () {
                                                $('#dostums_panel').toggle('slow');
                                            });

                                            $("#web_save").click(function () {
                                                var web_address = $('#web_address').val();
                                                var page_id = '<?php echo $new_page_id; ?>'
                                                $.post("./lib/fanpage.php", {'st': 5, 'web_address': web_address, 'page_id': page_id}, function (data) {
                                                    if (data == 1)
                                                    {
                                                        $('#web_panel').html(web_address);
                                                        alert('Congrats!!! Successful.');
                                                        $('#dostums_panel').toggle('slow');
                                                    }
                                                    else
                                                    {
                                                        alert('Sorry!!! Failed. Please Try Again.');
                                                    }
                                                });
                                            });
                                        });</script>



                                    <div class="last"> <!--Country restrictions start-->

                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="col-md-12"  id="date_from" style=" background:#F5F5F5; border: 1px #0cc solid; padding:15px;">
                                                    <div class="control-group">
                                                        <div class="col-md-4">
                                                            <label class="control-label" for="textinput"><strong>Country restrictions</strong></label>
                                                        </div>
                                                        <div class="col-md-8">

                                                            <div class="col-md-12 ">
                                                                <div class="col-md-10" id="Date_panel" style="padding-left:14px;">
                                                                    <?php $start_date = $obj->SelectAllByVal("dostums_fanpage", "page_id", $new_page_id, "start_date"); ?>

                                                                    <?php echo $start_date; ?>
                                                                </div>

                                                                <div class="col-md-2" style="padding-left:18px;">    
                                                                    <span style="color: #2C99CE; cursor: pointer;"> Edit</span><br>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--Country restrictions end-->




                                    <div class="col-md-12" id="start_date_panel" style="margin-bottom: 20px; display: none; border: 1px #69BD45 solid;background-color:#E6E6E6; padding: 10px 0;"> <!--Country restrictions hidden start-->

                                        <div class="col-md-4">
                                            <label class="control-label" for="textinput">
                                                <strong  style="padding-left:16px;">Country restrictions</strong>
                                            </label>
                                        </div>
                                        <div class="col-md-8">

                                            <input type="email" class="form-control" id="exampleInputname" placeholder=" Enter Country or countries">

                                            <div class="col-md-9" style="padding-left:20px; padding-top:10px;">                          
                                                <label class="radio-inline">
                                                    <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">Only show this Page to viewers in these countries
                                                </label><br>

                                                <label class="radio-inline">
                                                    <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">Hide this Page from viewers in these countries
                                                </label>
                                            </div>


                                            <div class="col-md-12" style="padding-left:0px;">
                                                <button type="submit" id="save_started_option" class="btn btn-success btn-sm">Save change</button>
                                                <button type="submit" class="btn btn-danger btn-sm" id="start_date_close">cancel</button>

                                            </div>



                                        </div>
                                    </div><!--Country restrictions hidden end-->

                                    <style>
                                        #Date_from,#start_date_close{cursor:pointer;}
                                        #start_date_close:hover{cursor:pointer;color:#000;}
                                    </style>

                                    <script>
                                        $('document').ready(function () {
                                            $('#date_from, #start_date_close').click(function () {
                                                $('#start_date_panel').toggle('slow');
                                            });

                                            $('#save_started_option').click(function () {

                                                var select_started_option = $('#select_started_option').val();
                                                var select_started_year = $('#select_started_year').val();
                                                var select_started_month = $('#select_started_month').val();
                                                var select_started_day = $('#select_started_day').val();
                                                var page_id = '<?php echo $new_page_id; ?>';
                                                //alert(select_started_option + ' ' + select_started_year + ' ' + select_started_month + ' ' + select_started_day);
                                                var start_date = select_started_option + ' : ' + select_started_day + '/' + select_started_month + '/' + select_started_year;
                                                $.post("./lib/fanpage.php", {'st': 14, 'start_date': start_date, 'page_id': page_id}, function (data) {
                                                    if (data == 1)
                                                    {
                                                        $('#Date_panel').html(start_date);
                                                        alert('Congrats!!! Successful.');
                                                        $('#start_date_panel').toggle('slow');
                                                    }
                                                    else
                                                    {
                                                        alert('Sorry!!! Failed. Please Try Again.');
                                                    }
                                                });
                                            });
                                        });

                                    </script>



                                    <div class="address2">
                                        <div class="last"> <!--Age restrictions start-->

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <div class="col-md-12"  id="address_from" style=" background:#F5F5F5; border: 1px #0cc solid; padding:16px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Age restrictions</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 "> 
                                                                    <div class="col-md-10" id="show_address" style="padding-left:14px;">
                                                                        <?php
                                                                        $thikana = $obj->SelectAllByVal("dostums_fanpage", "page_id", $new_page_id, "address");
                                                                        echo $thikana;
                                                                        ?>


                                                                    </div>

                                                                    <div class="col-md-2">    
                                                                        <span style="color: #2C99CE;"> Edit</span><br>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!--Age restrictions end-->



                                        <div class="col-md-12" id="address_panel" style="margin-bottom: 20px; display: none; border: 1px #69BD45 solid;background-color:#E6E6E6; padding: 10px 0;"> <!--Age restrictions  hidden start-->

                                            <div class="col-md-4">
                                                <label class="control-label" for="textinput">
                                                    <strong  style="padding-left:16px;"> Age restrictions </strong>

                                                </label>
                                            </div>
                                            <div class="col-md-8" >
                                                <div class="col-md-11 controls"style="padding-left:2px;">
                                                    <select class="form-control" id="select_started_option" style="background:#F5F5F5; width: 262px; margin-top:11px;">
                                                        <option value="Unspecified">Select Age Restrictions:</option>
                                                        <option value="Born">Select Age Restrictions</option>
                                                        <option value="Opened">Anyone (13+)</option>
                                                        <option value="Opened">People 17 and over</option>
                                                        <option value="Created">People 18 and over</option>
                                                        <option value="Launched">People 19 and over</option>
                                                        <option value="Launched">People 21 and over</option>
                                                        <option value="Launched">Alcohol-related</option>


                                                    </select>


                                                </div>

                                                <div class="col-md-12"style="padding-left:2px;">
                                                    <button type="submit" id="search_save" class="btn btn-success btn-sm">Save change</button>
                                                    <button type="submit" class="btn btn-danger btn-sm" id="address_close">cancel</button>

                                                </div>



                                            </div>


                                        </div><!--Age restrictions  hidden end-->
                                    </div><!--Age restrictions last end-->


                                    <style>
                                        #address_from,#address_close{cursor:pointer;}
                                        #address_close:hover{cursor:pointer;color:#000;}
                                    </style>

                                    <script>
                                        $('document').ready(function () {
                                            $('#address_from,#address_close').click(function () {
                                                $('#address_panel').toggle('slow');
                                            });
                                            $("#search_save").click(function () {
                                                var easy_address = $('#easy_address').val();
                                                var page_id = '<?php echo $new_page_id; ?>'
                                                $.post("./lib/fanpage.php", {'st': 6, 'easy_address': easy_address, 'page_id': page_id}, function (data) {
                                                    if (data == 1)
                                                    {
                                                        $('#show_address').html(easy_address);
                                                        alert('Congrats!!! Successful.');
                                                        $('#address_panel').toggle('slow');
                                                    }
                                                    else
                                                    {
                                                        alert('Sorry!!! Failed. Please Try Again.');
                                                    }
                                                });
                                            });
                                        });</script>












                                    <div id="all_everyone">
                                        <div class="last"> <!--Profanity filter start-->

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <div class="col-md-12"  id="short_from" style=" background:#F5F5F5; border: 1px #0cc solid; padding:12px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput" style="padding-left:4px;"><strong>Profanity filter </strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 " id="friend_requests_panel">
                                                                    <div class="col-md-10" id="short_decrip_value" style="padding-left:14px;">

                                                                        <?php
                                                                        $sortdecrib = $obj->SelectAllByVal("dostums_fanpage", "page_id", $new_page_id, "short_description");
                                                                        echo $sortdecrib;
                                                                        ?>
                                                                    </div>

                                                                    <div class="col-md-2" style="padding-left:10px;">    
                                                                        <span style="color: #2C99CE; cursor: pointer;"> Edit</span><br>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--Profanity filter end-->

                                            <div class="col-md-12" id="short_discrp_panel" style="margin-bottom: 20px; display: none; border: 1px #69BD45 solid;background-color:#E6E6E6; padding: 10px 0;"> <!--Profanity filter hidden start-->

                                                <div class="col-md-3">
                                                    <label class="control-label" for="textinput">
                                                        <strong  style="padding-left:16px;">Profanity filter</strong>
                                                    </label>
                                                </div>
                                                <div class="col-md-9" style="padding-left:30px;">

                                                    <div class="col-md-11 controls"style="padding-left:26px;">
                                                        <select class="form-control" id="select_started_option" style="background:#F5F5F5; width: 262px; margin-top:11px;">
                                                            <option value="Unspecified">Off</option>
                                                            <option value="Born">Medium</option>
                                                            <option value="Opened">Strong</option>
                                                         


                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="col-md-12" style="padding-left:193px;">
                                                    <button type="submit"  id="save_short_description" class="btn btn-success btn-sm">Save change</button>
                                                    <button type="submit" class="btn btn-danger btn-sm" id="sort_close">cancel</button>

                                                </div>

                                            </div><!--Profanity filter hidden end-->

                                        </div><!--Profanity filter 2end-->

                                        <style type="text/css">
                                            #follow_cmnt_close,#sort_close{ cursor: pointer; }
                                            #sort_close:hover{ cursor: pointer; color: #000; }
                                        </style>
                                        <script>
                                            $('document').ready(function () {
                                                $('#short_from, #sort_close').click(function () {
                                                    $('#short_discrp_panel').toggle('slow');
                                                });
                                                $("#save_short_description").click(function () {
                                                    // alert('dsdsfds');
                                                    var area_short = $('#area_short').val();
                                                    var page_id = '<?php echo $new_page_id; ?>'
                                                    $.post("./lib/fanpage.php", {'st': 7, 'area_short': area_short, 'page_id': page_id}, function (data) {
                                                        if (data == 1)
                                                        {
                                                            $('#short_decrip_value').html(area_short);
                                                            alert('Congrats!!! Successful.');
                                                            $('#short_discrp_panel').toggle('slow');
                                                        }
                                                        else
                                                        {
                                                            alert('Sorry!!! Failed. Please Try Again.');
                                                        }
                                                    });
                                                });
                                            });</script>


                                        <script>

                                        </script>



                                        <div class="remove">
                                            <div class="row form-group"> <!--Remove Page start-->
                                                <div class="col-md-12">
                                                    <div class="col-md-12" id="remove_plugin" style=" background:#F5F5F5; border: 1px #0cc solid; padding:15px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Remove Page</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 " >
                                                                    <div class="col-md-10" id="impressumdb_panel" href="#" style="padding-left:10px;">

                                                                        <?php
                                                                        $pressum = $obj->SelectAllByVal("dostums_fanpage", "page_id", $new_page_id, "impressum");
                                                                        echo $pressum;
                                                                        ?>
                                                                    </div>

                                                                    <div class="col-md-2">    
                                                                        <span style="color: #2C99CE;"> Edit</span><br>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!--Remove Page end-->

                                        <div class="col-md-12" id="remove_panel" style="margin-bottom: 20px; display: none; border: 1px #69BD45 solid;background-color:#E6E6E6; padding: 10px 0;"> <!--Remove Page  hidden start-->

                                            <div class="col-md-3">
                                                <label class="control-label" for="textinput">
                                                    <strong  style="padding-left:15px;">Remove Page</strong>
                                                </label>
                                            </div>
                                            <div class="col-md-9" >
                                                <div class="col-md-11"style="padding-left:31px;">


                                                </div>

                                                <div class="col-md-9 controls" style="padding-left:30px;">

                                                    Deleting your Page means that nobody will be able to see or find it. Once you click delete, 
                                                    you'll have 14 days to restore it in case you change your mind.
                                                    After that, you'll be asked to confirm whether to delete it permanently.
                                                    If you choose to unpublish instead, only admins will be able to see your Page.<br>

                                                    <button type="button" class="btn btn-link btn-sm" id="remove_page" data-toggle="modal" data-target="#remove_modal" style="padding-left:1px; color:#2CB8E8;">Delete Bhuyian Host</button>


                                                </div>



                                            </div>
                                            <div class="col-md-12"style="padding-left:180px;">
                                                <button type="submit" id="impressum_save" class="btn btn-success btn-sm">Save change</button>
                                                <button type="submit" class="btn btn-danger btn-sm" id="remove_plaug_close">cancel</button>

                                            </div>


                                            <!--modal start--->
                                            <div class="modal fade" tabindex="-1" role="dialog" id="remove_modal" style="padding-top:80px;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header"  style="background-color:#99CA3C;">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" style="font-size:24px;">Delete Page?</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>If you delete your Page, you'll still be able to restore it within 14 days. 
                                                                After that, you'll be asked to confirm that you want to permanently delete it. 
                                                                You can also select Unpublish this page below so that only admins can see this page.

                                                                Are you sure you want to begin the process of deleting this page?</p>
                                                        </div>
                                                        <div class="modal-footer">

                                                            <div class="checkbox pull-left" style="padding-left:20px;">
                                                                <label>
                                                                    <input type="checkbox">&nbsp;Unpublish this page.
                                                                </label>
                                                            </div>
                                                            <button type="button" class="btn btn-success" data-dismiss="modal">Delete Page&nbsp;?</button>
                                                            <button type="button" class="btn btn-primary">Cancel</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->

                                            <!--modal end--->



                                        </div><!--Remove Page hidden end-->
                                        <style>
                                            #remove_plugin,#remove_plaug_close{cursor:pointer;}
                                            #remove_plaug_close:hover{cursor:pointer;color:#000;}
                                        </style>

                                        <script>
                                            $('document').ready(function () {
                                                $('#remove_plugin, #remove_plaug_close').click(function () {
                                                    $('#remove_panel').toggle('slow');
                                                });

                                                $("#impressum_save").click(function () {
                                                    var impressumd_input = $('#impressumd_input').val();
                                                    var page_id = '<?php echo $new_page_id; ?>'
                                                    $.post("./lib/fanpage.php", {'st': 8, 'impressumd_input': impressumd_input, 'page_id': page_id}, function (data) {
                                                        if (data == 1)
                                                        {
                                                            $('#impressumdb_panel').html(impressumd_input);
                                                            alert('Congrats!!! Successful.');
                                                            $('#impressum_panel').toggle('slow');
                                                        }
                                                        else
                                                        {
                                                            alert('Sorry!!! Failed. Please Try Again.');
                                                        }
                                                    });
                                                });
                                            });</script>
                                    </div>
























                            </div>

                            <div style="clear:both;"></div>


                        </div>

                        <div class="panel-footer">



                        </div>


                    </div>
                </div>

            </div>

            <div class="col-md-3" style="padding-right: 0px;">
                <aside class="side-menu">

                    <?php
//profile user detail start
//include('plugin/profile_user_detail.php');
//profile user detail end
                    ?>


                    <?php
//friend list start
                    include('plugin/profile_frnd_list.php');
//friend list end
                    ?>


                    <?php
//Photo list start
                    include('plugin/profile_photo_list.php');
//Photo list end
                    ?>


                    <?php
//Like Pages list start
                    include('plugin/profile_like_pages_list.php');
//Like Pages list end
                    ?>

                    <?php
//Groups list start
                    include('plugin/profile_groups_list.php');
//Groups list end
                    ?>



                </aside>
            </div>
        </div>
    </div>

    <?php include('plugin/fotter.php') ?>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/material/dropdownjs/jquery.dropdown.js"></script>
    <script src="assets/material/js/ripples.min.js"></script>
    <script src="assets/material/js/material.min.js"></script>
    <script>
                                            $(document).ready(function () {
                                                $.material.init();
                                                $(".select").dropdown({"autoinit": ".select"});
                                            });
                                            $(document).ready(function () {

                                                $('#calendar-widget').fullCalendar({
                                                    contentHeight: 'auto',
                                                    theme: true,
                                                    header: {
                                                        right: '',
                                                        center: 'prev, title, next',
                                                        left: ''
                                                    },
                                                    defaultDate: '2014-06-12',
                                                    editable: true,
                                                    events: [
                                                        {
                                                            title: 'All ',
                                                            start: '2014-06-01',
                                                            className: 'bgm-cyan'
                                                        }

                                                    ]
                                                });
                                            });</script>


    <script src="assets/js/jquery.scrollto.js"></script>
    <script src="assets/js/jquery.easing.1.3.js"></script>
    <script src="assets/js/jquery.sticky.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/script.js"></script>
    <script src="assets/js/chat.js"></script>

</body>
</html>
