// JavaScript Document
$(document).ready(function () {

    $("#add_location").click(function () {

        $('#add_location').html("Loading...");
        $.get("http://ipinfo.io", function (response) {

            load_notification = {'st': 9, 'country_code': response.country};

            $.post('lib/status.php', load_notification, function (data) {
                if (data == 1)
                {
                    //$(this).val(response.country);
                    document.getElementById('add_location').innerHTML = response.country;
                }
                else
                {
                    alert("Something Went Wrong Please retry Again");
                    location.reload();
                }
            });
            //console.log(response.country);
        }, "jsonp");


    });

    $("#statusPermission").change(function () {
        var status = $(this).val();
        load_notification = {'st': 8, 'post_permission': status};
        $.post('lib/status.php', load_notification, function (data) {

        });
    });

    $("#btnPostStatus").click(function () {
        var msg = $('#status_message').val();
        load_notification = {'st': 1, 'msg': msg};
        $.post('lib/status.php', load_notification, function (data) {
            location.reload();
        });
    });

    $("#btnPostStatus_other").click(function () {
        var msg = $('#status_message').val();
        var to_user = $(this).attr('name');
        load_notification = {'st': 11, 'msg': msg, 'to_user': to_user};
        $.post('lib/status.php', load_notification, function (data) {
            location.reload();
        });
    });


    $(".sp-like").on('click', (function (e) {
        var post_id = $(this).attr('id');
        var place = "postlike" + post_id;
        load_data_like = {'st': 2, 'post_id': post_id};
        $.post('lib/status.php', load_data_like, function (data) {
            if (data == 1)
            {
                $('#' + place).fadeIn('slow');
                $('#' + place).html("Unlike");
                loadcommentautoload(post_id);
            }
            else if (data == 0)
            {
                $('#' + place).fadeIn('slow');
                $('#' + place).html("Like");
                loadcommentautoload(post_id);
            }
            /*else
             {
             window.location.reload();	
             }*/
        });

    }));

    $(".sp-mike-like").on('click', (function (e) {

        var post_id = $(this).attr('name');
        //alert('success'+post_id);
        var place = "mike-like-place";
        load_data_mike_like = {'st': 2, 'post_id': post_id};
        $.post('lib/status.php', load_data_mike_like, function (data) {
            if (data == 1)
            {
                $('#' + place).fadeIn('slow');
                $('#' + place).html("Unlike");
            }
            else if (data == 0)
            {
                $('#' + place).fadeIn('slow');
                $('#' + place).html("Like");
            }
            /*else
             {
             window.location.reload();	
             }*/
        });

    }));



    $(".sp-share").on('click', (function (e) {
        var post_id = $(this).attr('id');
        load_data_share = {'st': 4, 'post_id': post_id};
        $.post('lib/status.php', load_data_share, function (data) {
            if (data == 1)
            {
                alert(data);
            }
            else
            {
                alert("Something Went Wrong Please retry Again");
            }
        });

    }));

    $(".dostums-post-delete").on('click', (function (e) {
        var post_id = $(this).attr('id');
        post_delete = {'st': 6, 'post_id': post_id};
        $.post('lib/status.php', post_delete, function (data) {
            if (data == 1)
            {
                alert(data);
            }
            else
            {
                alert("Something Went Wrong Please retry Again");
            }
        });

    }));

    $(".post-type").on('click', (function (e) {
        $('.post-types li').attr('class', "post-type");
        $(this).attr("class", "post-type active");
    }));

    $("#uploadImage").on('click', (function (e) {
        e.preventDefault();
        $('#uploadStatusImage').trigger('click');
    }));




});

function UploadImageString(time)
{
    var img = $('#img' + time).attr('src')
    var post_message = $('#status_message').val();
    load_data_image = {'st': 3, 'img': img, 'post': post_message};
    $.post('lib/image.php', load_data_image, function (data)
    {
        $('#imagestatusboxshow').hide();
        $('#post_button').html("<input type='button' id='btnPostStatus' class='btn btn-post btn-success pull-right no-margin' value='Post' name='submit'>");
        location.reload();
    });
}

function UploadImageStringOther(time, to_user)
{
    var img = $('#img' + time).attr('src')
    var post_message = $('#status_message').val();
    load_data_image = {'st': 4, 'to_user': to_user, 'img': img, 'post': post_message};
    $.post('lib/image.php', load_data_image, function (data)
    {
        $('#imagestatusboxshow').hide();
        $('#post_button').html("<input type='button' id='btnPostStatus_other' class='btn btn-post btn-success pull-right no-margin' value='Post' name='submit'>");
        location.reload();
    });
}


function frndrequest(user_id, statusplace, reqtype)
{
    load_data_like = {'st': reqtype, 'usrid': user_id};
    $.post('lib/friend.php', load_data_like, function (data) {
        if (data == 1)
        {
            alert(reqtype);
            if (reqtype == 1)
            {
                $('#' + statusplace).html("<span class='btn btn-sm btn-warning'><i class='fa fa-close'></i> Cancel</span>");
                $('#suggest' + user_id).hide();
            }
            else if (reqtype == 2)
            {
                $('#' + statusplace).html("<span class='btn btn-sm btn-success'><i class='fa fa-close'></i> Friend</span>");
                $('#suggest' + user_id).hide();
            }
            else if (reqtype == 3)
            {
                $('#' + statusplace).html("<span class='btn btn-sm btn-info'><i class='fa fa-user-plus'></i> Request</span>");
                $('#suggest' + user_id).hide();
            }
        }

    });
}

function frndsearch(user_id, reqtype)
{
    load_data_like = {'st': reqtype, 'usrid': user_id};
    $.post('lib/friend.php', load_data_like, function (data) {
        if (data == 1)
        {
            if (reqtype == 1)
            {
                $('#searchfrnd_' + user_id).html("<i class='fa fa-user-times margin-right10'></i>Cancel Request");
                $('#searchfrnd_' + user_id).attr("onclick", "frndsearch(" + user_id + ",3)");
            }
            else if (reqtype == 2)
            {
                $('#searchfrnd_' + user_id).html("<i class='fa fa-user margin-right10'></i>Friends");
                $('#searchfrnd_' + user_id).attr("onclick", "frndsearch(" + user_id + ",3)");
            }
            else if (reqtype == 3)
            {
                $('#searchfrnd_' + user_id).html("<i class='fa fa-user-plus margin-right10'></i>Add Friend");
                $('#searchfrnd_' + user_id).attr("onclick", "frndsearch(" + user_id + ",1)");
            }
        }

    });
}

function frndconfirm(user_id, reqtype)
{
    load_data_like = {'st': reqtype, 'usrid': user_id};
    $.post('lib/friend.php', load_data_like, function (data) {
        if (data == 1)
        {
            if (reqtype == 1)
            {
                $('#ff_'+user_id).hide('slow');
            }
            else if (reqtype == 2)
            {
                $('#ff_'+user_id).hide('slow');
            }
            else if (reqtype == 3)
            {
                $('#ff_'+user_id).hide('slow');
            }
        }

    });
}

function frndrequest_Profile(user_id, statusplace, reqtype)
{
    load_data_like = {'st': reqtype, 'usrid': user_id};
    $.post('lib/friend.php', load_data_like, function (data) {

        if (reqtype == 1)
        {
            $('#' + statusplace).html("<i class='fa fa-close'></i> Request Sent");
            $('#' + statusplace).attr("onclick", "frndrequest_Profile('" + user_id + "','profile_request_status_" + user_id + "','3')");
        }
        else if (reqtype == 2)
        {
            $('#' + statusplace).html("<i class='fa fa-user'></i> Friend");
            $('#' + statusplace).attr("onclick", "frndrequest_Profile('" + user_id + "','profile_request_status_" + user_id + "','3')");
        }
        else if (reqtype == 3)
        {
            $('#' + statusplace).html("<i class='fa fa-user-plus'></i> add friend");
            $('#' + statusplace).attr("onclick", "frndrequest_Profile('" + user_id + "','profile_request_status_" + user_id + "','1')");
        }


    });
}

function likeComment(comment_id, post_id)
{
    load_data_like = {'st': 5, 'comment_id': comment_id, 'post_id': post_id};
    $.post('lib/status.php', load_data_like, function (data) {
        //console.log(data);
        var datacl = jQuery.parseJSON(data);
        var std = datacl.status;
        var like = datacl.like;
        var place = "bcomlikes" + post_id + "_" + comment_id;
        var places = "comlikes" + post_id + "_" + comment_id;
        if (std == 1)
        {
            if (like != 0)
            {
                $('#' + places).fadeIn('slow');
                $('#' + places).html("Unlike");
                $('#' + place).attr("title", like + " People Likes.");
            }
            else
            {
                $('#' + places).fadeIn('slow');
                $('#' + places).html("Unlike");
                $('#' + place).attr("title", "");
            }
        }
        else
        {
            if (like != 0)
            {
                $('#' + places).fadeIn('slow');
                $('#' + places).html("Like");
                $('#' + place).attr("title", like + " People Likes.");
            }
            else
            {

                $('#' + places).fadeIn('slow');
                $('#' + places).html("Like");
                $('#' + place).attr("title", "");

            }
        }

    });
}

function deleteComment(comment_id, post_id)
{
    load_data_delete = {'st': 7, 'comment_id': comment_id};
    $.post('lib/status.php', load_data_delete, function (data) {
        if (data == 1)
        {
            $('#com' + comment_id).fadeOut('slow');
            loadcommentautoload(post_id);
        }
        else
        {
            alert("Something Went Wrong Please retry Again");
            loadcommentautoload(post_id)
        }
    });
}



function loadcommentautoload(post_id)
{
    load_new_comment = {'st': 1, 'post_id': post_id};
    $.post('lib/comment.php', load_new_comment, function (comment) {
        if (comment)
        {
            $('#comment_list_instant_load_' + post_id).fadeIn('slow');
            $('#comment_list_instant_load_' + post_id).html(comment);
        }
        else
        {
            window.location.refresh();
        }
    });

    load_count_comment = {'st': 3, 'post_id': post_id};
    $.post('lib/comment.php', load_count_comment, function (commentc) {

        var globaldataconds = false;
        var datacl = jQuery.parseJSON(commentc);
        var like = datacl.likes;
        var commentd = datacl.comment;
        var globalcomlik = (like - 0) + (commentd - 0);
        if (globalcomlik != 0)
        {
            globaldataconds = true;
        }

        if (globaldataconds)
        {

            if (globalcomlik != 0)
            {


                $('#loadallcomment' + post_id).css('display', 'inline-block');
                $('#mcc' + post_id).fadeIn('slow');
                if (like == 0)
                {
                    $('#mcc' + post_id).html(commentd + " comments");
                }
                else if (commentd == 0)
                {
                    $('#mcc' + post_id).html(like + " likes");
                }
                else if (like != 0 && commentd != 0)
                {
                    $('#mcc' + post_id).html(like + " likes and " + commentd + " comments");

                }
                else
                {
                    $('#loadallcomment' + post_id).css('display', 'none');
                    $('#mcc' + post_id).fadeIn('slow');
                }

                if (commentd != 0)
                {
                    $('#postcomment' + post_id).html(commentd);
                }
                else
                {
                    $('#postcomment' + post_id).html(" ");
                }


            }
            else
            {
                $('#loadallcomment' + post_id).fadeOut('slow');
                $('#loadallcomment' + post_id).css('display', 'none');
            }

        }
        else
        {
            //location.reload();
            $('#loadallcomment' + post_id).fadeOut('slow');
            $('#loadallcomment' + post_id).css('display', 'none');
        }



    });
}

function comment(post_id)
{
    var msg = $('#content' + post_id).val();
    load_data_comment = {'st': 3, 'post_id': post_id, 'msg': msg};
    load_new_comment = {'st': 1, 'post_id': post_id};
    $.post('lib/status.php', load_data_comment, function (data) {
        if (data == 1)
        {
            $("#cancel" + post_id).click();
            $.post('lib/comment.php', load_new_comment, function (comment) {
                if (comment)
                {
                    loadcommentautoload(post_id);
                }
                else
                {
                    window.location.refresh();
                }
            });
        }
        else
        {
            alert("Something Went Wrong Please retry Again");
        }
    });

}

function loadallcomment(post_id)
{

    load_data_comment = {'st': 2, 'post_id': post_id};
    $.post('lib/comment.php', load_data_comment, function (comment) {
        if (comment)
        {
            $('#comment_list_instant_load_' + post_id).html(comment);
            $('#loadallcomment' + post_id).css("display", "none");
        }
        else
        {
            alert("Something Went Wrong Please retry Again");
            window.location.refresh();
        }
    });

}