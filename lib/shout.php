<?php
include('../class/auth.php');
extract($_POST);
if ($st == 1) {
    $chknotification = $obj->FlyQuery("SELECT count(`id`) AS `total_frnd` FROM `dostums_friend` WHERE `to_uid`='" . $input_by . "' AND status=0");
    echo json_encode(array("friend_request" => $chknotification[0]->total_frnd));
} elseif ($st == 2) {
    $sqlrequest_detail = $obj->FlyQuery("
	SELECT alldata.* FROM (select a.id, 
	a.uid, 
	a.status, 
	IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
	concat(du.first_name,' ',du.last_name) as name,
	dc.country_name,
	du.city_id
	FROM 
	dostums_friend as a 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=a.uid AND dpp.status='2' LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id 
	LEFT JOIN dostums_user as du ON du.id=a.uid
	LEFT JOIN dostums_country as dc ON dc.id=du.country_id
	WHERE 
	a.to_uid='" . $input_by . "' 
	AND a.status='0') as alldata WHERE alldata.uid!=" . $input_by . "");
    if (!empty($sqlrequest_detail)) {
        $count = count($sqlrequest_detail);
        foreach ($sqlrequest_detail as $detail):
            echo '<li><div>
			<div class="dropdown-messages-box">
				<div class="pull-left">
                    
                    <img class="media-object img-circle img-thumbnail" alt="Image"
                         src="./profile/' . $detail->photo . '" style="height:50px; width:50px;">
                </div>
	
				<div class="media-body ">
					<small class="pull-right"><button class="btn btn-primary btn-sm" type="button"  id="friend_request_status_' . $detail->uid . '"  onclick="frndrequest_Profile(' . $detail->uid . ',friend_request_status_' . $detail->uid . ',2)">Approve</button></small>
					<strong style="margin-top:10px;">
					<a href="profile.php?user_id=' . $detail->uid . '">' . $detail->name . '</small></a>
					</strong>
					<br><small>' . $detail->city_id . '&nbsp;,&nbsp;' . $detail->country_name . ' 
	
				</div>
			</div>
		</div>
		</li>';

            echo '<li class="divider"></li>';


        endforeach;

        echo '<li><a href="#">
			<div class="dropdown-messages-box">
	
				<div class="media-body text-center">

					<a href="friend-requests.php">Visit Friend Request Page</a> 
	
				</div>
			</div>
		</a>
		</li>';
    }
    else {
        echo '<li><a href="#">
			<div class="dropdown-messages-box">
	
				<div class="media-body ">

					<strong>No Request Found Please Reload Page Again.</strong> 
	
				</div>
			</div>
		</a>
		</li>
		<li class="divider"></li>';
    }
} elseif ($st == 3) {
    $chknotification = $obj->FlyQuery("SELECT count(`id`) AS `total_frnd` FROM `dostums_friend` WHERE `to_uid`='" . $usrid . "' AND status=2");
    echo json_encode(array("friend_request" => $chknotification[0]->total_frnd));
} elseif ($st == 4) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->SelectAllByID_Multiple("dostums_friend", array("to_uid" => $usrid, "status" => 2));
    if (!empty($chknotification))
        foreach ($chknotification as $noti) {
            ?>
            <!-- START list group item-->
            <a class="list-group-item" href="profile.php?user_id=<?php echo $noti->uid; ?>">
                <div class="media">
                    <div class="pull-left">
                        <?php
                        $photo_id = $obj->SelectAllByVal2("dostums_profile_photo", "user_id", $noti->uid, "status", 2, "photo_id");
                        $photo = $obj->SelectAllByVal("dostums_photo", "id", $photo_id, "photo");

                        if ($photo == "") {
                            $new_photo = "generic-man-profile.jpg";
                        } else {
                            $new_photo = $photo;
                        }
                        ?>
                        <img class="media-object img-circle thumb48" alt="Image"
                             src="./profile/<?php echo $new_photo; ?>">
                    </div>
                    <div class="media-body clearfix">
                        <strong class="media-heading text-primary">
                            <span class="circle circle-success circle-lg text-left"></span>
                            <?php echo $obj->SelectAllByVal("dostums_user_view", "id", $noti->uid, "name"); ?>
                        </strong>

                        <p class="mb-sm">
                            <small><?php echo $extra->duration(($noti->date) . " 00:00:00", date('Y-m-d H:i:s')); ?></small>
                        </p>
                    </div>
                </div>
            </a>
            <!-- END list group item-->
            <?php
        }
} elseif ($st == 5) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->SelectAllByID_Multiple("dostums_friend", array("to_uid" => $usrid, "status" => 2));
    if (!empty($chknotification))
        foreach ($chknotification as $noti) {
            ?>
            <!-- START list group item-->
            <a class="list-group-item" href="profile.php?user_id=<?php echo $noti->uid; ?>">
                <div class="media">
                    <div class="pull-left">

                        <img class="media-object img-circle thumb48" alt="Image"
                             src="./profile/<?php echo $new_photo; ?>">
                    </div>
                    <div class="media-body clearfix">
                        <strong class="media-heading text-primary">
                            <span class="circle circle-success circle-lg text-left"></span>
                            <?php echo $obj->SelectAllByVal("dostums_user_view", "id", $noti->uid, "name"); ?>
                        </strong>

                        <p class="mb-sm">
                            <small><?php echo $extra->duration(($noti->date) . " 00:00:00", date('Y-m-d H:i:s')); ?></small>
                        </p>
                    </div>
                </div>
            </a>
            <!-- END list group item-->
            <li>
                <a class="item clearfix" name="1" href="#">
                    <?php
                    $photo = $obj->SelectAllByVal("dostums_photo", "id", $obj->SelectAllByVal2("dostums_profile_photo", "user_id", $noti->uid, "status", 2, "photo_id"), "photo");

                    if ($photo == "") {
                        $new_photo = "generic-man-profile.jpg";
                    } else {
                        $new_photo = $photo;
                    }
                    ?>
                    <img class="img" alt="img" src="./profile/<?php echo $new_photo; ?>">
                    <span class="from"><?php echo $obj->SelectAllByVal("dostums_user_view", "id", $noti->uid, "name"); ?></span>
                    Hello, m8 how is goin ?
                    <span class="date">22 May</span>
                </a>
            </li>

            <?php
        }
} elseif ($st == 6) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->FlyQuery("SELECT alldata.* FROM (select a.id, 
	a.uid, 
	a.status, 
	IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
	concat(du.first_name,' ',du.last_name) as name,
	dc.country_name,
	du.city_id,
	dua.about_short,
	dua.occupation,
	dua.company,
	dei.school,
	dei.college,
	dei.university1,
	dei.university2,
        dfbl.to_uid,
        dfbl.status as block_status
	FROM 
	dostums_friend as a 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=a.uid AND dpp.status='2' 
        LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id 
	LEFT JOIN dostums_user as du ON du.id=a.uid
	LEFT JOIN dostums_country as dc ON dc.id=du.country_id
	LEFT JOIN dostums_user_about as dua ON dua.user_id=a.uid
	LEFT JOIN dostums_educational_institutions as dei ON dei.user_id=a.uid
        LEFT JOIN dostums_friends_blocklist as dfbl on dfbl.to_uid=a.uid
	WHERE 
	a.to_uid='" . $usrid . "' 
	AND a.status='2') as alldata WHERE alldata.uid!='" . $usrid . "'");
    //$chkstatus=$obj->SelectAllByVal2("dostums_friends_blocklist","uid",$input_by,"to_uid",$usrid,"status");

    if (!empty($chknotification))
        foreach ($chknotification as $noti) {

            if ($noti->block_status != 1) {
                ?>
                <!-- START list group item-->
                <div id="friendslistsingle_<?php echo $noti->id; ?>" class="list-group-item friend-list-item col-md-6" style="background-color:#B3E5FC; width:48%; margin:1%;"><!--width:48%; margin-right:1%;-->
                    <div class="media clearfix">
                        <div class="pull-left">

                            <img class="media-object thumb48 img-thumbnail" alt="Image"
                                 src="./profile/<?php echo $noti->photo; ?>">
                        </div>
                        <div class="media-body clearfix">
                            <div class="pull-right"><div class="dropdown">
                                    <span href="" data-toggle="dropdown" aria-expanded="false" class="btn btn-sm btn-danger">
                                        <i class="fa fa-star"></i> Friend <i class="fa fa-caret-down"></i>
                                    </span>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <!-- <li>
                                             <a href="">    Get Notifications
                 
                                             </a>
                                         </li>
                                         <li>
                                             <a href=""> <i class="fa close-friend-icon color-success fa-check"></i> Close Friends
                                             </a>
                                         </li>
                                         <li>
                                             <a href="">  Acquaintances
                 
                                             </a>
                                         </li>-->
                                        <li>
                                            <button onclick="FriendsBlock('<?php echo $noti->uid; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-default btn-sm">
                                                block

                                            </button>
                                        </li>
                                        <li class="divider" role="presentation"></li>
                                        <li>
                                            <button onclick="FriendsUnfriend('<?php echo $noti->uid; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-default btn-sm">
                                                Unfriend

                                            </button>
                                        </li>

                                    </ul>
                                </div>
                            </div>




                            <a class="link" href="profile.php?user_id=<?php echo $noti->uid; ?>">
                                <strong class="media-heading text-primary">
                                    <?php echo $noti->name; ?>
                                </strong>
                            </a>

                            <div class="frined-info">

                                <p class="mb-sm"  data-placement="bottom" data-toggle="tooltip" data-original-title="Mutual friends include Rj Naim  and 34 more...">
                                    <small>53 mutual friends</small>
                                </p>
                                <p><br></p>
                                <p>About&nbsp;:  <a class="link" href=""><?php echo $noti->about_short; ?></a></p>
                                <p>Lives in&nbsp;:   <a class="link" href=""><i class="fa fa-map-marker">&nbsp;</i><?php echo $noti->city_id; ?>, <?php echo $noti->country_name; ?></a></p>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END list group item-->
                <?php
            }
        }
} elseif ($st == 7) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->FlyQuery("SELECT alldata.* FROM (select a.id, 
	a.uid, 
	a.status, 
	IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
	concat(du.first_name,' ',du.last_name) as name,
	dc.country_name,
	du.city_id,
	dua.about_short,
	dua.occupation,
	dua.company,
	dei.school,
	dei.college,
	dei.university1,
	dei.university2
	FROM 
	dostums_friend as a 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=a.uid AND dpp.status='2' LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id 
	LEFT JOIN dostums_user as du ON du.id=a.uid
	LEFT JOIN dostums_country as dc ON dc.id=du.country_id
	LEFT JOIN dostums_user_about as dua ON dua.user_id=a.uid
	LEFT JOIN dostums_educational_institutions as dei ON dei.user_id=a.uid
	WHERE 
	a.to_uid='" . $usrid . "' 
	AND a.status='0') as alldata WHERE alldata.uid!='" . $usrid . "'");
    if (!empty($chknotification))
        foreach ($chknotification as $noti) {

            if ($noti->name != '') {
                ?>
                <div class="col-sm-12" id="ff_<?php echo $noti->uid; ?>">
                    <div class="list-group-item friend-list-item">
                        <div class="media clearfix">
                            <div class="pull-left">

                                <img class="media-object thumb48 img-thumbnail" alt="Image" src="./profile/<?php echo $noti->photo; ?>">
                            </div>
                            <div class="media-body clearfix">

                                <div class="pull-right"><div class="">
                                        <span onclick="frndconfirm(<?php echo $noti->uid; ?>, 2)" id="searchfrnd_<?php echo $noti->uid; ?>"  class="frqst-btn btn btn-sm btn-success">
                                            Confirm
                                        </span>

                                        <span onclick="frndconfirm(<?php echo $noti->uid; ?>, 3)" id="searchfrnd_<?php echo $noti->uid; ?>"  class="frqst-btn btn btn-sm btn-danger">
                                            Delete Request
                                        </span>


                                    </div>
                                </div>




                                <a class="link" href="profile.php?user_id=<?php echo $noti->uid; ?>">
                                    <strong class="media-heading text-primary">
                                        <?php echo $noti->name; ?>
                                    </strong>
                                </a>


                                <div class="frined-info">

                                                                                            <!--<p class="mb-sm"  >
                                                                                                Ismail Shah and <span class="link" data-placement="bottom" data-toggle="tooltip" data-original-title="Mutual friends include Rj Naim Maisha Quorishi Ranjit Roy It'z Sumon Saiful Islam Sudhir Halder Ferdous Alam Perbes Shekh Raisha Jahan Rupa Shamim Kazi Shakhawat  and 34 more...">9 other mutual friends</span>   <small></small>
                                                                                            </p>-->
                                    <p><br></p>
                                    <p> 
                                        <?php
                                        if ($noti->company != '') {
                                            echo '<i class="mdi-action-work"></i> Works  at<a class="link" href="">' . $noti->company . '</a>';
                                        }
                                        ?>


                                    </p>
                                    <p>
                                        <?php
                                        if ($noti->university2 != '') {
                                            echo '<i class="mdi-social-school"></i><a class="link" href="">' . $noti->university2 . '</a>';
                                        } elseif ($noti->university1 != '') {
                                            echo '<i class="mdi-social-school"></i><a class="link" href="">' . $noti->university1 . '</a>';
                                        } elseif ($noti->college != '') {
                                            echo '<i class="mdi-social-school"></i><a class="link" href="">' . $noti->college . '</a>';
                                        } elseif ($noti->school != '') {
                                            echo '<i class="mdi-social-school"></i><a class="link" href="">' . $noti->school . '</a>';
                                        }
                                        ?>
                                    </p>
                                    <p> 
                                        <?php
                                        if ($noti->city_id != '' && $noti->country_name != '') {
                                            echo '<i class="mdi-communication-location-on"></i>Lives in<a class="link" href="">' . $noti->city_id . '&nbsp;,&nbsp;' . $noti->country_name . '</a>';
                                        } elseif ($noti->country_name != '') {
                                            echo '<i class="mdi-communication-location-on"></i>Lives in<a class="link" href="">' . $noti->country_name . '</a>';
                                        } elseif ($noti->city_id != '') {
                                            echo '<i class="mdi-communication-location-on"></i>Lives in<a class="link" href="">' . $noti->city_id . '</a>';
                                        }
                                        ?>
                                    </p>


                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
} elseif ($st == 8) {
    $chknotification = $obj->FlyQuery("SELECT count(`id`) AS `total_frnd_reqst` FROM `dostums_friend` WHERE `to_uid`='" . $usrid . "' AND status=0");
    echo json_encode(array("friend_request" => $chknotification[0]->total_frnd_reqst));
} elseif ($st == 9) {
    $chknotification = $obj->FlyQuery("SELECT count(`id`) AS `total_group_member` FROM `dostums_group_members` WHERE `group_id`='" . $group_id . "' AND status='1'");
    echo json_encode(array("ttl_group_mem" => $chknotification[0]->total_group_member));
} elseif ($st == 10) {
    $sqlrequest_detail = $obj->FlyQuery("
	SELECT a.id,
	a.group_id,
	a.user_id,
	a.input_by,
	a.date_time,
	a.status,
	IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
	concat(du.first_name,' ',du.last_name) as name
	FROM dostums_group_members AS a
	LEFT JOIN dostums_user as du ON du.id=a.user_id 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=a.user_id 
	LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id  
	WHERE a.`group_id`='group_id' 
	AND a.status=1");
    if (!empty($sqlrequest_detail)) {
        $count = count($sqlrequest_detail);
        foreach ($sqlrequest_detail as $detail):
            echo '<a class="list-group-item" href="./profile.php?user_id=' . $detail->user_id . '">
            <div class="media">
                <div class="pull-left">
                    <img class="media-object img-circle thumb48" alt="Image"
                         src="./profile/' . $detail->photo . '">
                </div>
                <div class="media-body clearfix">
                    <small class="pull-right "><span class="btn btn-sm btn-danger"><i
                            class="fa fa-user-plus"></i> Add </span></small>
                    <strong class="media-heading text-primary">
                        <span class="circle circle-success circle-lg text-left"></span>
						' . $detail->name . '
					</strong>

                    <p class="mb-sm">
                        <small>' . $detail->date_time . '</small>
                    </p>
                </div>
            </div>
        </a>';




        endforeach;
    }
}
elseif ($st == 11) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->FlyQuery("SELECT 
alldata.*,
IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
concat(du.first_name,' ',du.last_name) as name,
dc.country_name,
du.city_id,
dua.about_short,
dua.occupation,
dua.company,
dei.school,
dei.college,
dei.university1,
dei.university2 
FROM 
	(SELECT a.id,
	a.group_id,
	a.user_id,
	a.input_by,
	a.date_time,
	a.status
	FROM dostums_group_members AS a
	WHERE a.status='1') as alldata 
	LEFT JOIN dostums_user as du ON du.id=alldata.user_id 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=alldata.user_id AND dpp.status='2'
	LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id
	LEFT JOIN dostums_country as dc ON dc.id=du.country_id
	LEFT JOIN dostums_user_about as dua ON dua.user_id=alldata.user_id
	LEFT JOIN dostums_educational_institutions as dei ON dei.user_id=alldata.user_id
	WHERE alldata.group_id='" . $group_id . "' GROUP BY alldata.user_id");
    if (!empty($chknotification))
        foreach ($chknotification as $noti) {
            ?>
            <!-- START list group item-->
            <div id="grp-mem-rbgrid_<?php echo $noti->id; ?>" class="list-group-item friend-list-item col-md-6" style="background-color:#B3E5FC; width:48%; margin:1%;"><!--width:48%; margin-right:1%;-->
                <div class="media clearfix">
                    <a href="profile.php?user_id=<?php echo $noti->user_id; ?>" class="pull-left">

                        <img class="media-object thumb48 img-thumbnail" alt="Image"
                             src="./profile/<?php echo $noti->photo; ?>">
                    </a>
                    <div class="media-body clearfix">
                        <div class="pull-right"><div class="dropdown">
                                <span href="" data-toggle="dropdown" aria-expanded="false" class="btn btn-sm btn-danger">
                                    <i class="fa fa-star"></i><!-- Member -->&nbsp;<i class="fa fa-caret-down"></i>
                                </span>

                                <ul class="dropdown-menu dropdown-menu-right text-center">
                                    <!--<li>
                                        <a href="">    Get Notifications

                                        </a>
                                    </li>
                                    <li>
                                        <a href=""> <i class="fa close-friend-icon color-success fa-check"></i> Close Friends
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">  Acquaintances

                                        </a>
                                    </li>
                                    <li class="divider" role="presentation"></li>-->
                                    <li>
                                        <button onclick="GroupMemRemove('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-default btn-sm">
                                            Remove From Group
                                        </button>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <button onclick="GroupMemBlock('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-default btn-sm">
                                            Block From Group
                                        </button>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <button onclick="MakeGroupAdmin('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-default btn-sm">
                                            Make Group Admin
                                        </button>
                                    </li>

                                </ul>
                            </div>
                        </div>




                        <a class="link" href="profile.php?user_id=<?php echo $noti->user_id; ?>">
                            <strong class="media-heading text-primary">
                                <?php echo $noti->name; ?>
                            </strong>
                        </a>

                        <div class="frined-info">

                                                                <!--<p class="mb-sm"  data-placement="bottom" data-toggle="tooltip" data-original-title="Mutual friends include Rj Naim  and 34 more...">
                                                                    <small>53 mutual friends</small>
                                                                </p>-->
                            <p><br></p>
                            <p>About&nbsp;:  <a class="link" href=""><?php echo $noti->about_short; ?></a></p>
                            <p>Lives in&nbsp;:   <a class="link" href="">&nbsp;</i><?php echo $noti->city_id; ?>, <?php echo $noti->country_name; ?></a></p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END list group item-->
            <?php
        }
} elseif ($st == 12) {
    $chknotification = $obj->FlyQuery("SELECT count(`id`) AS `total_group_member_request` FROM `dostums_group_members` WHERE `group_id`='" . $group_id . "' AND status='3'");
    echo json_encode(array("ttl_group_mem_reqst" => $chknotification[0]->total_group_member_request));
} elseif ($st == 13) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->FlyQuery("SELECT 
        alldata.*,
        IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
        concat(du.first_name,' ',du.last_name) as name,
        dc.country_name,
        du.city_id,
        dua.about_short,
        dua.occupation,
        dua.company,
        dei.school,
        dei.college,
        dei.university1,
        dei.university2 
        FROM 
	(SELECT a.id,
	a.group_id,
	a.user_id,
	a.input_by,
	a.date_time,
	a.status
	FROM dostums_group_members AS a
	WHERE a.status='3') as alldata 
	LEFT JOIN dostums_user as du ON du.id=alldata.user_id 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=alldata.user_id AND dpp.status='2'
	LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id
	LEFT JOIN dostums_country as dc ON dc.id=du.country_id
	LEFT JOIN dostums_user_about as dua ON dua.user_id=alldata.user_id
	LEFT JOIN dostums_educational_institutions as dei ON dei.user_id=alldata.user_id
	WHERE alldata.group_id='" . $group_id . "' GROUP BY alldata.user_id");
    if (!empty($chknotification))
        foreach ($chknotification as $noti) {
            ?>
            <!-- START list group item-->
            <div id="rqst-grp-mem-sgrid_<?php echo $noti->id; ?>" class="list-group-item friend-list-item col-md-6" style="background-color:#B3E5FC; width:48%; margin:1%;"><!--width:48%; margin-right:1%;-->
                <div class="media clearfix">
                    <a href="profile.php?user_id=<?php echo $noti->user_id; ?>" class="pull-left">

                        <img class="media-object thumb48 img-thumbnail" alt="Image"
                             src="./profile/<?php echo $noti->photo; ?>">
                    </a>
                    <div class="media-body clearfix">
                        <div class="pull-right"><div class="dropdown">
                                <span href="" data-toggle="dropdown" aria-expanded="false" class="btn btn-sm btn-danger">
                                    <i class="fa fa-star"></i><!-- Member -->&nbsp;<i class="fa fa-caret-down"></i>
                                </span>

                                <ul class="dropdown-menu dropdown-menu-right text-center">
                                    <!--<li>
                                        <a href="">    Get Notifications

                                        </a>
                                    </li>
                                    <li>
                                        <a href=""> <i class="fa close-friend-icon color-success fa-check"></i> Close Friends
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">  Acquaintances

                                        </a>
                                    </li>
                                    <li class="divider" role="presentation"></li>-->
                                    <li>
                                        <button onclick="GroupRqstConfirm('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-default btn-sm">
                                            Confirm Request
                                        </button>
                                    </li>
                                    <li class="divider" role="presentation"></li>
                                    <li>
                                        <button onclick="GroupRqstReject('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-default btn-sm">
                                            Reject Request
                                        </button>
                                    </li>

                                </ul>
                            </div>
                        </div>




                        <a class="link" href="profile.php?user_id=<?php echo $noti->user_id; ?>">
                            <strong class="media-heading text-primary">
                                <?php echo $noti->name; ?>
                            </strong>
                        </a>

                        <div class="frined-info">

                                                                <!--<p class="mb-sm"  data-placement="bottom" data-toggle="tooltip" data-original-title="Mutual friends include Rj Naim  and 34 more...">
                                                                    <small>53 mutual friends</small>
                                                                </p>-->
                            <p><br></p>
                            <p>About&nbsp;:  <a class="link" href=""><?php echo $noti->about_short; ?></a></p>
                            <p>Lives in&nbsp;:   <a class="link" href="">&nbsp;</i><?php echo $noti->city_id; ?>, <?php echo $noti->country_name; ?></a></p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END list group item-->
            <?php
        }
} elseif ($st == 14) {
    $chknotification = $obj->FlyQuery("SELECT count(`id`) AS `total_group_member_blocked` FROM `dostums_group_members` WHERE `group_id`='" . $group_id . "' AND status='4'");
    echo json_encode(array("ttl_group_mem_blocked" => $chknotification[0]->total_group_member_blocked));
} elseif ($st == 15) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->FlyQuery("SELECT 
        alldata.*,
        IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
        concat(du.first_name,' ',du.last_name) as name,
        dc.country_name,
        du.city_id,
        dua.about_short,
        dua.occupation,
        dua.company,
        dei.school,
        dei.college,
        dei.university1,
        dei.university2 
        FROM 
	(SELECT a.id,
	a.group_id,
	a.user_id,
	a.input_by,
	a.date_time,
	a.status
	FROM dostums_group_members AS a
	WHERE a.status='4') as alldata 
	LEFT JOIN dostums_user as du ON du.id=alldata.user_id 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=alldata.user_id AND dpp.status='2'
	LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id
	LEFT JOIN dostums_country as dc ON dc.id=du.country_id
	LEFT JOIN dostums_user_about as dua ON dua.user_id=alldata.user_id
	LEFT JOIN dostums_educational_institutions as dei ON dei.user_id=alldata.user_id
	WHERE alldata.group_id='" . $group_id . "' GROUP BY alldata.user_id");
    if (!empty($chknotification))
        foreach ($chknotification as $noti) {
            ?>
            <!-- START list group item-->
            <div id="rqst-grp-mem-bgrid_<?php echo $noti->id; ?>" class="list-group-item friend-list-item col-md-6" style="background-color:#B3E5FC; width:48%; margin:1%;"><!--width:48%; margin-right:1%;-->
                <div class="media clearfix">
                    <a href="profile.php?user_id=<?php echo $noti->user_id; ?>" class="pull-left">

                        <img class="media-object thumb48 img-thumbnail" alt="Image"
                             src="./profile/<?php echo $noti->photo; ?>">
                    </a>
                    <div class="media-body clearfix">
                        <div class="pull-right"><div class="dropdown">
                                <span href="" data-toggle="dropdown" aria-expanded="false" class="btn btn-sm btn-danger">
                                    <i class="fa fa-star"></i><!-- Member -->&nbsp;<i class="fa fa-caret-down"></i>
                                </span>

                                <ul class="dropdown-menu dropdown-menu-right text-center">
                                    <!--<li>
                                        <a href="">    Get Notifications

                                        </a>
                                    </li>
                                    <li>
                                        <a href=""> <i class="fa close-friend-icon color-success fa-check"></i> Close Friends
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">  Acquaintances

                                        </a>
                                    </li>
                                    <li class="divider" role="presentation"></li>-->
                                    <li>
                                        <button onclick="GroupBlkdMemUnblock('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-default btn-sm">
                                            Unblock Member
                                        </button>
                                    </li>
                                    <li class="divider" role="presentation"></li>
                                    <li>
                                        <button onclick="GroupBlkdMemRemove('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-default btn-sm">
                                            Remove Member from Group
                                        </button>
                                    </li>

                                </ul>
                            </div>
                        </div>




                        <a class="link" href="profile.php?user_id=<?php echo $noti->user_id; ?>">
                            <strong class="media-heading text-primary">
                                <?php echo $noti->name; ?>
                            </strong>
                        </a>

                        <div class="frined-info">

                                                                <!--<p class="mb-sm"  data-placement="bottom" data-toggle="tooltip" data-original-title="Mutual friends include Rj Naim  and 34 more...">
                                                                    <small>53 mutual friends</small>
                                                                </p>-->
                            <p><br></p>
                            <p>About&nbsp;:  <a class="link" href=""><?php echo $noti->about_short; ?></a></p>
                            <p>Lives in&nbsp;:   <a class="link" href="">&nbsp;</i><?php echo $noti->city_id; ?>, <?php echo $noti->country_name; ?></a></p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END list group item-->
            <?php
        }
} elseif ($st == 16) {
    $chknotification = $obj->FlyQuery("SELECT count(`id`) AS `ttl_group_admin` FROM `dostums_group_admin` WHERE `group_id`='" . $group_id . "' AND status='1'");
    echo json_encode(array("ttl_group_admin" => $chknotification[0]->ttl_group_admin));
} elseif ($st == 17) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->FlyQuery("SELECT 
        alldata.*,
        IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
        concat(du.first_name,' ',du.last_name) as name,
        dc.country_name,
        du.city_id,
        dua.about_short,
        dua.occupation,
        dua.company,
        dei.school,
        dei.college,
        dei.university1,
        dei.university2 
        FROM 
	(SELECT a.id,
	a.group_id,
	a.user_id,
	a.date,
	a.status
	FROM dostums_group_admin AS a
	WHERE a.status='1') as alldata 
	LEFT JOIN dostums_user as du ON du.id=alldata.user_id 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=alldata.user_id AND dpp.status='2'
	LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id
	LEFT JOIN dostums_country as dc ON dc.id=du.country_id
	LEFT JOIN dostums_user_about as dua ON dua.user_id=alldata.user_id
	LEFT JOIN dostums_educational_institutions as dei ON dei.user_id=alldata.user_id
	WHERE alldata.group_id='" . $group_id . "' GROUP BY alldata.user_id");
    if (!empty($chknotification))
        foreach ($chknotification as $noti) {
            ?>
            <!-- START list group item-->
            <div id="rqst-grp-mem-bgrid_<?php echo $noti->id; ?>" class="list-group-item friend-list-item col-md-6" style="background-color:#B3E5FC; width:48%; margin:1%;"><!--width:48%; margin-right:1%;-->
                <div class="media clearfix">
                    <a href="profile.php?user_id=<?php echo $noti->user_id; ?>" class="pull-left">

                        <img class="media-object thumb48 img-thumbnail" alt="Image"
                             src="./profile/<?php echo $noti->photo; ?>">
                    </a>
                    <div class="media-body clearfix">
                        <div class="pull-right"><div class="dropdown">
                                <span href="" data-toggle="dropdown" aria-expanded="false" class="btn btn-sm btn-danger">
                                    <i class="fa fa-star"></i><!-- Member -->&nbsp;<i class="fa fa-caret-down"></i>
                                </span>

                                <ul class="dropdown-menu dropdown-menu-right text-center">
                                    <!--<li>
                                        <a href="">    Get Notifications

                                        </a>
                                    </li>
                                    <li>
                                        <a href=""> <i class="fa close-friend-icon color-success fa-check"></i> Close Friends
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">  Acquaintances

                                        </a>
                                    </li>
                                    <li class="divider" role="presentation"></li>-->
                                    <li>
                                        <button onclick="GroupBlkdMemUnblock('<?php //echo $noti->user_id;    ?>', '<?php //echo $noti->id;    ?>')" type="button" class="btn btn-default btn-sm">
                                            Leave Group
                                        </button>
                                    </li>
                                    <li class="divider" role="presentation"></li>
                                    <li>
                                        <button onclick="GroupBlkdMemRemove('<?php //echo $noti->user_id;    ?>', '<?php //echo $noti->id;    ?>')" type="button" class="btn btn-default btn-sm">
                                            Remove Member from Group
                                        </button>
                                    </li>

                                </ul>
                            </div>
                        </div>




                        <a class="link" href="profile.php?user_id=<?php echo $noti->user_id; ?>">
                            <strong class="media-heading text-primary">
                                <?php echo $noti->name; ?>
                            </strong>
                        </a>

                        <div class="frined-info">

                                                                <!--<p class="mb-sm"  data-placement="bottom" data-toggle="tooltip" data-original-title="Mutual friends include Rj Naim  and 34 more...">
                                                                    <small>53 mutual friends</small>
                                                                </p>-->
                            <p><br></p>
                            <p>About&nbsp;:  <a class="link" href=""><?php echo $noti->about_short; ?></a></p>
                            <p>Lives in&nbsp;:   <a class="link" href="">&nbsp;</i><?php echo $noti->city_id; ?>, <?php echo $noti->country_name; ?></a></p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END list group item-->
            <?php
        }
} elseif ($st == 18) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->FlyQuery("SELECT 
        a.user_id,
        a.page_id,
        a.status,
        concat(du.first_name,' ',du.last_name) as name,
        IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
        a.date
        FROM dostums_page_likes as a 
        LEFT JOIN dostums_user as du ON du.id=a.user_id
        LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=a.user_id AND dpp.status='2'
        LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id
        WHERE a.page_id='" . $page_id . "' AND a.status='1'");
    if (!empty($chknotification))
        foreach ($chknotification as $noti) {
            ?>
            <!-- START list group item-->
            <div class="list-group-item friend-list-item col-md-6" style="background-color:#B3E5FC; width:48%; margin:1%;"><!--width:48%; margin-right:1%;-->
                <div class="media clearfix">
                    <div class="pull-left">

                        <img class="media-object thumb48 img-thumbnail" alt="Image"
                             src="./profile/<?php echo $noti->photo; ?>">
                    </div>
                    <div class="media-body clearfix">
                        <!--        				<div class="pull-right"><div class="dropdown">
                                                    <span href="" data-toggle="dropdown" aria-expanded="false" class="btn btn-sm btn-danger">
                                                        <i class="fa fa-star"></i> Friend <i class="fa fa-caret-down"></i>
                                                    </span>
                                
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li>
                                                            <a href="">    Get Notifications
                                
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href=""> <i class="fa close-friend-icon color-success fa-check"></i> Close Friends
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="">  Acquaintances
                                
                                                            </a>
                                                        </li>
                                                        <li class="divider" role="presentation"></li>
                                                        <li>
                                                            <a href="">
                                                                Unfriend
                                
                                                            </a>
                                                        </li>
                                
                                                    </ul>
                                                </div>
                                             </div>-->




                        <a class="link" href="profile.php?user_id=<?php echo $noti->user_id; ?>">
                            <strong class="media-heading text-primary">
                                <?php echo $noti->name; ?>
                            </strong>
                        </a>

                        <div class="frined-info">

                            <p class="mb-sm"  data-placement="bottom" data-toggle="tooltip" data-original-title="Mutual friends include Rj Naim  and 34 more...">
                                <small>53 mutual friends</small>
                            </p>
                            <p><br></p>
                            <p>About&nbsp;:  <a class="link" href=""><?php //echo $noti->about_short;    ?></a></p>
                            <p>Lives in&nbsp;:   <a class="link" href=""><i class="fa fa-map-marker">&nbsp;</i><?php //echo $noti->city_id;    ?>, <?php //echo $noti->country_name;    ?></a></p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END list group item-->
            <?php
        }
} elseif ($st == 19) {
    $chknotification = $obj->FlyQuery("SELECT count(`id`) AS `total_followers` FROM `dostums_page_likes` WHERE `page_id`='" . $page_id . "' AND status=1");
    echo json_encode(array("total_followers_list" => $chknotification[0]->total_followers));
} elseif ($st == 20) {
    $query = "SELECT count(`id`) AS `total_invitations` FROM `dostums_page_likes` WHERE `user_id`='" . $usrid . "' AND status=2";
    $chknotification = $obj->FlyQuery($query);
    $varnew = array("total_invitations_list" => $chknotification[0]->total_invitations);
    echo json_encode($varnew);
    //echo $chknotification[0]->total_invitations;
} elseif ($st == 21) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->FlyQuery("SELECT 
        alldata.*,
        IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
        concat(du.first_name,' ',du.last_name) as name,
        dc.country_name,
        du.city_id,
        dua.about_short,
        dua.occupation,
        dua.company,
        dei.school,
        dei.college,
        dei.university1,
        dei.university2 
        FROM 
	(SELECT a.id,
	a.group_id,
	a.user_id,
	a.date,
	a.status
	FROM dostums_group_admin AS a
	WHERE a.status='1') as alldata 
	LEFT JOIN dostums_user as du ON du.id=alldata.user_id 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=alldata.user_id AND dpp.status='2'
	LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id
	LEFT JOIN dostums_country as dc ON dc.id=du.country_id
	LEFT JOIN dostums_user_about as dua ON dua.user_id=alldata.user_id
	LEFT JOIN dostums_educational_institutions as dei ON dei.user_id=alldata.user_id
	WHERE alldata.group_id='" . $group_id . "' GROUP BY alldata.user_id");
    if (!empty($chknotification))
        foreach ($chknotification as $noti) {
            ?>
            <!-- START list group item-->
            <div id="rqst-grp-mem-bgrid_<?php echo $noti->id; ?>" class="list-group-item friend-list-item col-sm-12" style="background-color:#B3E5FC; margin-top: 10px;"><!--width:48%; margin-right:1%;-->
                <div class="media clearfix">
                    <a href="profile.php?user_id=<?php echo $noti->user_id; ?>" class="pull-left">

                        <img class="media-object thumb48 img-thumbnail" alt="Image"
                             src="./profile/<?php echo $noti->photo; ?>">
                    </a>
                    <div class="media-body clearfix">
                        <div class="pull-right"><div class="dropdown">
                                <span href="" data-toggle="dropdown" aria-expanded="false" class="btn btn-sm btn-danger">
                                    <i class="fa fa-star"></i><!-- Member -->&nbsp;<i class="fa fa-caret-down"></i>
                                </span>

                                <ul class="dropdown-menu dropdown-menu-right text-center">
                                    <!--<li>
                                        <a href="">    Get Notifications

                                        </a>
                                    </li>
                                    <li>
                                        <a href=""> <i class="fa close-friend-icon color-success fa-check"></i> Close Friends
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">  Acquaintances

                                        </a>
                                    </li>
                                    <li class="divider" role="presentation"></li>-->
                                    <li>
                                        <button onclick="GroupBlkdMemUnblock('<?php //echo $noti->user_id;    ?>', '<?php //echo $noti->id;    ?>')" type="button" class="btn btn-sm">
                                            Leave Group
                                        </button>
                                    </li>
                                    <li class="divider" role="presentation"></li>
                                    <li>
                                        <button onclick="GroupBlkdMemRemove('<?php //echo $noti->user_id;    ?>', '<?php //echo $noti->id;    ?>')" type="button" class="btn btn-sm">
                                            Remove Member from Group
                                        </button>
                                    </li>

                                </ul>
                            </div>
                        </div>




                        <a class="link" href="profile.php?user_id=<?php echo $noti->user_id; ?>">
                            <strong class="media-heading text-primary">
                                <?php echo $noti->name; ?>
                            </strong>
                        </a>

                        <div class="frined-info">

                                                                <!--<p class="mb-sm"  data-placement="bottom" data-toggle="tooltip" data-original-title="Mutual friends include Rj Naim  and 34 more...">
                                                                    <small>53 mutual friends</small>
                                                                </p>-->
                            <p><br></p>
                            <p>About&nbsp;:  <a class="link" href=""><?php echo $noti->about_short; ?></a></p>
                            <p>Lives in&nbsp;:   <a class="link" href="">&nbsp;</i><?php echo $noti->city_id; ?>, <?php echo $noti->country_name; ?></a></p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END list group item-->
            <?php
        }
} elseif ($st == 22) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->FlyQuery("SELECT 
alldata.*,
IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
concat(du.first_name,' ',du.last_name) as name,
dc.country_name,
du.city_id,
dua.about_short,
dua.occupation,
dua.company,
dei.school,
dei.college,
dei.university1,
dei.university2 
FROM 
	(SELECT a.id,
	a.group_id,
	a.user_id,
	a.input_by,
	a.date_time,
	a.status
	FROM dostums_group_members AS a
	WHERE a.status='1') as alldata 
	LEFT JOIN dostums_user as du ON du.id=alldata.user_id 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=alldata.user_id AND dpp.status='2'
	LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id
	LEFT JOIN dostums_country as dc ON dc.id=du.country_id
	LEFT JOIN dostums_user_about as dua ON dua.user_id=alldata.user_id
	LEFT JOIN dostums_educational_institutions as dei ON dei.user_id=alldata.user_id
	WHERE alldata.group_id='" . $group_id . "' GROUP BY alldata.user_id");
    if (!empty($chknotification))
        foreach ($chknotification as $noti) {
            ?>
            <!-- START list group item-->
            <div id="grp-mem-rbgrid_<?php echo $noti->id; ?>" class="list-group-item friend-list-item col-sm-12" style="background-color:#B3E5FC; margin-top: 10px;"><!--width:48%; margin-right:1%;-->
                <div class="media clearfix">
                    <a href="profile.php?user_id=<?php echo $noti->user_id; ?>" class="pull-left">

                        <img class="media-object thumb48 img-thumbnail" alt="Image"
                             src="./profile/<?php echo $noti->photo; ?>">
                    </a>
                    <div class="media-body clearfix">
                        <div class="pull-right"><div class="dropdown">
                                <span href="" data-toggle="dropdown" aria-expanded="false" class="btn btn-sm btn-danger">
                                    <i class="fa fa-star"></i><!-- Member -->&nbsp;<i class="fa fa-caret-down"></i>
                                </span>

                                <ul class="dropdown-menu dropdown-menu-right text-center">
                                    <!--<li>
                                        <a href="">    Get Notifications

                                        </a>
                                    </li>
                                    <li>
                                        <a href=""> <i class="fa close-friend-icon color-success fa-check"></i> Close Friends
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">  Acquaintances

                                        </a>
                                    </li>
                                    <li class="divider" role="presentation"></li>-->
                                    <li>
                                        <button onclick="GroupMemRemove('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-sm">
                                            Remove From Group
                                        </button>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <button onclick="GroupMemBlock('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-sm">
                                            Block From Group
                                        </button>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <button onclick="MakeGroupAdmin('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-sm">
                                            Make Group Admin
                                        </button>
                                    </li>

                                </ul>
                            </div>
                        </div>




                        <a class="link" href="profile.php?user_id=<?php echo $noti->user_id; ?>">
                            <strong class="media-heading text-primary">
                                <?php echo $noti->name; ?>
                            </strong>
                        </a>

                        <div class="frined-info">

                                                                <!--<p class="mb-sm"  data-placement="bottom" data-toggle="tooltip" data-original-title="Mutual friends include Rj Naim  and 34 more...">
                                                                    <small>53 mutual friends</small>
                                                                </p>-->
                            <p><br></p>
                            <p>About&nbsp;:  <a class="link" href=""><?php echo $noti->about_short; ?></a></p>
                            <p>Lives in&nbsp;:   <a class="link" href="">&nbsp;</i><?php echo $noti->city_id; ?>, <?php echo $noti->country_name; ?></a></p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END list group item-->
            <?php
        }
} elseif ($st == 23) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->FlyQuery("SELECT 
        alldata.*,
        IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
        concat(du.first_name,' ',du.last_name) as name,
        dc.country_name,
        du.city_id,
        dua.about_short,
        dua.occupation,
        dua.company,
        dei.school,
        dei.college,
        dei.university1,
        dei.university2 
        FROM 
	(SELECT a.id,
	a.group_id,
	a.user_id,
	a.input_by,
	a.date_time,
	a.status
	FROM dostums_group_members AS a
	WHERE a.status='4') as alldata 
	LEFT JOIN dostums_user as du ON du.id=alldata.user_id 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=alldata.user_id AND dpp.status='2'
	LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id
	LEFT JOIN dostums_country as dc ON dc.id=du.country_id
	LEFT JOIN dostums_user_about as dua ON dua.user_id=alldata.user_id
	LEFT JOIN dostums_educational_institutions as dei ON dei.user_id=alldata.user_id
	WHERE alldata.group_id='" . $group_id . "' GROUP BY alldata.user_id");
    if (!empty($chknotification))
        foreach ($chknotification as $noti) {
            ?>
            <!-- START list group item-->
            <div id="rqst-grp-mem-bgrid_<?php echo $noti->id; ?>" class="list-group-item friend-list-item col-sm-12" style="background-color:#B3E5FC; margin-top: 10px;"><!--width:48%; margin-right:1%;-->
                <div class="media clearfix">
                    <a href="profile.php?user_id=<?php echo $noti->user_id; ?>" class="pull-left">

                        <img class="media-object thumb48 img-thumbnail" alt="Image"
                             src="./profile/<?php echo $noti->photo; ?>">
                    </a>
                    <div class="media-body clearfix">
                        <div class="pull-right"><div class="dropdown">
                                <span href="" data-toggle="dropdown" aria-expanded="false" class="btn btn-sm btn-danger">
                                    <i class="fa fa-star"></i><!-- Member -->&nbsp;<i class="fa fa-caret-down"></i>
                                </span>

                                <ul class="dropdown-menu dropdown-menu-right text-center">
                                    <!--<li>
                                        <a href="">    Get Notifications

                                        </a>
                                    </li>
                                    <li>
                                        <a href=""> <i class="fa close-friend-icon color-success fa-check"></i> Close Friends
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">  Acquaintances

                                        </a>
                                    </li>
                                    <li class="divider" role="presentation"></li>-->
                                    <li>
                                        <button onclick="GroupBlkdMemUnblock('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-sm">
                                            Unblock Member
                                        </button>
                                    </li>
                                    <li class="divider" role="presentation"></li>
                                    <li>
                                        <button onclick="GroupBlkdMemRemove('<?php echo $noti->user_id; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-sm">
                                            Remove Member from Group
                                        </button>
                                    </li>

                                </ul>
                            </div>
                        </div>




                        <a class="link" href="profile.php?user_id=<?php echo $noti->user_id; ?>">
                            <strong class="media-heading text-primary">
                                <?php echo $noti->name; ?>
                            </strong>
                        </a>

                        <div class="frined-info">

                                                                <!--<p class="mb-sm"  data-placement="bottom" data-toggle="tooltip" data-original-title="Mutual friends include Rj Naim  and 34 more...">
                                                                    <small>53 mutual friends</small>
                                                                </p>-->
                            <p><br></p>
                            <p>About&nbsp;:  <a class="link" href=""><?php echo $noti->about_short; ?></a></p>
                            <p>Lives in&nbsp;:   <a class="link" href="">&nbsp;</i><?php echo $noti->city_id; ?>, <?php echo $noti->country_name; ?></a></p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END list group item-->
            <?php
        }
} elseif ($st == 24) {
    //$chknotification=$obj->FlyQuery("SELECT count(`id`) AS `ttl_friends_blocked` FROM `dostums_friends_blocklist` WHERE `user_id`='".$usrid."' AND status='1'");
    //echo json_encode(array("ttl_friends_blocked"=>$chknotification[0]->ttl_friends_blocked));

    $query = "SELECT count(`id`) AS `ttl_friends_blocked` FROM `dostums_friends_blocklist` WHERE `uid`='" . $input_by . "' AND status=1";
    $chknotification = $obj->FlyQuery($query);

    //echo "Block FOund ".$chknotification[0]->ttl_friends_blocked;

    $varnews = array("ttl_friends_blocked" => $chknotification[0]->ttl_friends_blocked);
    echo json_encode($varnews);
} elseif ($st == 25) {
    include('../class/extraClass.php');
    $extra = new SiteExtra();
    $chknotification = $obj->FlyQuery("SELECT alldata.* FROM (select a.id, 
	a.uid, 
	a.status, 
	IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
	concat(du.first_name,' ',du.last_name) as name,
	dc.country_name,
	du.city_id,
	dua.about_short,
	dua.occupation,
	dua.company,
	dei.school,
	dei.college,
	dei.university1,
	dei.university2,
        dfbl.to_uid,
        dfbl.status as block_status
	FROM 
	dostums_friend as a 
	LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=a.uid AND dpp.status='2' 
        LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id 
	LEFT JOIN dostums_user as du ON du.id=a.uid
	LEFT JOIN dostums_country as dc ON dc.id=du.country_id
	LEFT JOIN dostums_user_about as dua ON dua.user_id=a.uid
	LEFT JOIN dostums_educational_institutions as dei ON dei.user_id=a.uid
        LEFT JOIN dostums_friends_blocklist as dfbl on dfbl.to_uid=a.uid
	WHERE 
	a.to_uid='" . $usrid . "' 
	AND a.status='2' AND dfbl.status='1') as alldata WHERE alldata.uid!='" . $usrid . "'");
    //$chkstatus=$obj->SelectAllByVal2("dostums_friends_blocklist","uid",$input_by,"to_uid",$usrid,"status");

    if (!empty($chknotification))
        foreach ($chknotification as $noti) {

            //if ($noti->block_status != 1) {
            ?>
            <!-- START list group item-->
            <div id="friendslistsingle_<?php echo $noti->id; ?>" class="list-group-item friend-list-item col-md-6" style="background-color:#B3E5FC; width:48%; margin:1%;"><!--width:48%; margin-right:1%;-->
                <div class="media clearfix">
                    <div class="pull-left">

                        <img class="media-object thumb48 img-thumbnail" alt="Image"
                             src="./profile/<?php echo $noti->photo; ?>">
                    </div>
                    <div class="media-body clearfix">
                        <div class="pull-right"><div class="dropdown">
                                <span href="" data-toggle="dropdown" aria-expanded="false" class="btn btn-sm btn-danger">
                                    <i class="fa fa-star"></i> Friend <i class="fa fa-caret-down"></i>
                                </span>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <!-- <li>
                                         <a href="">    Get Notifications
             
                                         </a>
                                     </li>
                                     <li>
                                         <a href=""> <i class="fa close-friend-icon color-success fa-check"></i> Close Friends
                                         </a>
                                     </li>
                                     <li>
                                         <a href="">  Acquaintances
             
                                         </a>
                                     </li>-->
                                    <li>
                                        <button onclick="FriendsUnblock('<?php echo $noti->uid; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-default btn-sm">
                                            Unblock
                                        </button>
                                    </li>
                                    <li class="divider" role="presentation"></li>
                                    <li>
                                        <button onclick="FriendsUnfriend('<?php echo $noti->uid; ?>', '<?php echo $noti->id; ?>')" type="button" class="btn btn-default btn-sm">
                                            Unfriend
                                        </button>
                                    </li>

                                </ul>
                            </div>
                        </div>




                        <a class="link" href="profile.php?user_id=<?php echo $noti->uid; ?>">
                            <strong class="media-heading text-primary">
                                <?php echo $noti->name; ?>
                            </strong>
                        </a>

                        <div class="frined-info">

                            <p class="mb-sm"  data-placement="bottom" data-toggle="tooltip" data-original-title="Mutual friends include Rj Naim  and 34 more...">
                                <small>53 mutual friends</small>
                            </p>
                            <p><br></p>
                            <p>About&nbsp;:  <a class="link" href=""><?php echo $noti->about_short; ?></a></p>
                            <p>Lives in&nbsp;:   <a class="link" href=""><i class="fa fa-map-marker">&nbsp;</i><?php echo $noti->city_id; ?>, <?php echo $noti->country_name; ?></a></p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END list group item-->
            <?php
            //}
        }
} else {
    echo 0;
}
?>








