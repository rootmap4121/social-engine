<?php
	include('../class/auth.php');	
	include('../class/uploadImage_Class.php');
	$imagelib=new image_class();
	extract($_POST);
	define('UPLOAD_DIR', '../profile/');
	$img = $_POST['img'];
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace('data:image/jpeg;base64,', '', $img);
	$img = str_replace('data:image/gif;base64,', '', $img);
	$img = str_replace('data:image/bmp;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	
	$new_data=explode(";",$_POST['img']);
	$type=substr($new_data[0],11,100);
	
	$image_name=uniqid() . '.'.$type;
	$file = UPLOAD_DIR . $image_name;		
	$success = file_put_contents($file, $data);
	//print $success ? $file : 'Unable to save the file.';
	
	
	if($success)
	{
		if($st==1)
		{
			$profile_photo=$imagelib->upload_imageFromString("135","135",UPLOAD_DIR,$file,"profile",$data);
			$obj->insert("dostums_photo",array("photo"=>$profile_photo,"photo2"=>$image_name,"status"=>1));
			$photo_ids=$obj->SelectAllByVal2("dostums_photo","photo",$profile_photo,"photo2",$image_name,"id");
			$obj->insert("dostums_post", array("user_id" =>$input_by,"post" =>"  Changed Profile Photo ","photo_id" =>$photo_ids, "post_time" => date('Y-m-d H:i:s'),"date" =>date('Y-m-d'),"status" =>2));
			$post_id=$obj->SelectAllByVal("dostums_post","photo_id",$photo_ids,"id");
			$obj->update("dostums_profile_photo",array("user_id"=>$input_by,"status"=>1));
			$obj->insert("dostums_profile_photo",array("user_id"=>$input_by,"photo_id"=>$photo_ids,"date"=>date('Y-m-d'),"status"=>2));
			
			$permission=$obj->SelectAllByVal2("dostums_post_permission","user_id",$input_by,"date",date('Y-m-d'),"post_permission");
		
			if($permission=="")
			{
				$new_permission=1;	
			}
			else
			{
				$new_permission=$permission;
			}
			
			$obj->insert("dostums_post_permission_record",array("user_id"=>$input_by,"post_id"=>$post_id,"permission_id"=>$new_permission,"date"=>date('Y-m-d'),"status"=>1));
	$get_country_ids=$obj->FlyQuery("SELECT country_id FROM dostums_post_location_record WHERE user_id='".$input_by."' AND date='".date('Y-m-d')."' ORDER by id DESC LIMIT 1");
			$get_country_id=$get_country_ids[0]->country_id;
			
			$obj->insert("dostums_post_location_record",array("user_id"=>$input_by,"post_id"=>$post_id,"country_id"=>$get_country_id,"date"=>date('Y-m-d'),"status"=>1));
			
			
			echo 1;
			//$obj->insert("notification", array("uid" =>$input_by,"message" =>" Change Profile Photo ","link_id" =>$post_id,"date" =>date('Y-m-d'),"status" =>24));
		}
		elseif($st==2)
		{
			$profile_photo=$imagelib->upload_imageFromString("960","315",UPLOAD_DIR,$file,"cover",$data);
			$photo_ids=$obj->insertAndReturnID("dostums_photo",array("photo"=>$profile_photo,"photo2"=>$image_name,"status"=>1));
			$post_id=$obj->insertAndReturnID("dostums_post", array("user_id" =>$input_by,"post" =>"  Changed Cover Photo ","photo_id" =>$photo_ids, "post_time" => date('Y-m-d H:i:s'),"date" =>date('Y-m-d'),"status" =>2));
			$obj->update("dostums_cover_photo",array("user_id"=>$input_by,"status"=>1));
			$obj->insert("dostums_cover_photo",array("user_id"=>$input_by,"photo_id"=>$photo_ids,"date"=>date('Y-m-d'),"status"=>2));
			
			
			
			$permission=$obj->SelectAllByVal2("dostums_post_permission","user_id",$input_by,"date",date('Y-m-d'),"post_permission");
		
			if($permission=="")
			{
				$new_permission=1;	
			}
			else
			{
				$new_permission=$permission;
			}
			
			$obj->insert("dostums_post_permission_record",array("user_id"=>$input_by,"post_id"=>$post_id,"permission_id"=>$new_permission,"date"=>date('Y-m-d'),"status"=>1));
	$get_country_ids=$obj->FlyQuery("SELECT country_id FROM dostums_post_location_record WHERE user_id='".$input_by."' AND date='".date('Y-m-d')."' ORDER by id DESC LIMIT 1");
			$get_country_id=$get_country_ids[0]->country_id;
			
			$obj->insert("dostums_post_location_record",array("user_id"=>$input_by,"post_id"=>$post_id,"country_id"=>$get_country_id,"date"=>date('Y-m-d'),"status"=>1));
			
			echo $profile_photo;
		}
		elseif($st==3)
		{
			$profile_photo=$imagelib->uploadFiximageFromString(UPLOAD_DIR,$file,"status",$data);
			$photo_ids=$obj->insertAndReturnID("dostums_photo",array("photo"=>$profile_photo,"photo2"=>$image_name,"status"=>1));
			$post_id=$obj->insertAndReturnID("dostums_post", array("user_id" =>$input_by,"post" =>$post,"photo_id" =>$photo_ids, "post_time" => date('Y-m-d H:i:s'),"date" =>date('Y-m-d'),"status" =>3));
			
			
			$permission=$obj->SelectAllByVal2("dostums_post_permission","user_id",$input_by,"date",date('Y-m-d'),"post_permission");
		
			if($permission=="")
			{
				$new_permission=1;	
			}
			else
			{
				$new_permission=$permission;
			}
			
			$obj->insert("dostums_post_permission_record",array("user_id"=>$input_by,"post_id"=>$post_id,"permission_id"=>$new_permission,"date"=>date('Y-m-d'),"status"=>1));
	$get_country_ids=$obj->FlyQuery("SELECT country_id FROM dostums_post_location_record WHERE user_id='".$input_by."' AND date='".date('Y-m-d')."' ORDER by id DESC LIMIT 1");
			$get_country_id=$get_country_ids[0]->country_id;
			
			$obj->insert("dostums_post_location_record",array("user_id"=>$input_by,"post_id"=>$post_id,"country_id"=>$get_country_id,"date"=>date('Y-m-d'),"status"=>1));
			
			
			echo $profile_photo;
		}
		elseif($st==4)
		{
			$profile_photo=$imagelib->uploadFiximageFromString(UPLOAD_DIR,$file,"status",$data);
			$photo_ids=$obj->insertAndReturnID("dostums_photo",array("photo"=>$profile_photo,"photo2"=>$image_name,"status"=>1));
			$post_id=$obj->insertAndReturnID("dostums_post", array("user_id" =>$input_by,"to_user_id"=>$to_user,"post" =>$post,"photo_id" =>$photo_ids, "post_time" => date('Y-m-d H:i:s'),"date" =>date('Y-m-d'),"status" =>4));
			echo $profile_photo;
		}
		elseif($st==5)
		{
			$profile_photo=$imagelib->upload_imageFromString("135","135",UPLOAD_DIR,$file,"group_profile",$data);
			$obj->insert("dostums_photo",array("photo"=>$profile_photo,"photo2"=>$image_name,"status"=>1));
			$photo_ids=$obj->SelectAllByVal2("dostums_photo","photo",$profile_photo,"photo2",$image_name,"id");
			$obj->insert("dostums_post", array("user_id" =>$input_by,"post" =>"  Changed Profile Photo ","photo_id" =>$photo_ids, "post_time" => date('Y-m-d H:i:s'),"date" =>date('Y-m-d'),"status" =>60));
			$post_id=$obj->SelectAllByVal("dostums_post","photo_id",$photo_ids,"id");
			$obj->updateUsingMultiple("dostums_group_profile_photo",array("status"=>1),array("user_id"=>$input_by,"group_id"=>$group_id));
			$obj->insert("dostums_group_profile_photo",array("user_id"=>$input_by,"group_id"=>$group_id,"photo_id"=>$photo_ids,"date"=>date('Y-m-d'),"status"=>2));
			
			$permission=$obj->SelectAllByVal2("dostums_post_permission","user_id",$input_by,"date",date('Y-m-d'),"post_permission");
		
			if($permission=="")
			{
				$new_permission=1;	
			}
			else
			{
				$new_permission=$permission;
			}
			
			$obj->insert("dostums_post_permission_record",array("user_id"=>$input_by,"post_id"=>$post_id,"permission_id"=>$new_permission,"date"=>date('Y-m-d'),"status"=>1));
	$get_country_ids=$obj->FlyQuery("SELECT country_id FROM dostums_post_location_record WHERE user_id='".$input_by."' AND date='".date('Y-m-d')."' ORDER by id DESC LIMIT 1");
			$get_country_id=$get_country_ids[0]->country_id;
			
			$obj->insert("dostums_post_location_record",array("user_id"=>$input_by,"post_id"=>$post_id,"country_id"=>$get_country_id,"date"=>date('Y-m-d'),"status"=>1));
			
			
			echo 1;
			//$obj->insert("notification", array("uid" =>$input_by,"message" =>" Change Profile Photo ","link_id" =>$post_id,"date" =>date('Y-m-d'),"status" =>24));
		}
		elseif($st==6)
		{
			$profile_photo=$imagelib->upload_imageFromString("960","315",UPLOAD_DIR,$file,"group_cover",$data);
			$photo_ids=$obj->insertAndReturnID("dostums_photo",array("photo"=>$profile_photo,"photo2"=>$image_name,"status"=>1));
			$post_id=$obj->insertAndReturnID("dostums_post", array("user_id" =>$input_by,"post" =>"  Changed Cover Photo ","photo_id" =>$photo_ids, "post_time" => date('Y-m-d H:i:s'),"date" =>date('Y-m-d'),"status" =>61));
			$obj->updateUsingMultiple("dostums_group_cover_photo",array("status"=>1),array("user_id"=>$input_by,"group_id"=>$group_id));
			$obj->insert("dostums_group_cover_photo",array("user_id"=>$input_by,"group_id"=>$group_id,"photo_id"=>$photo_ids,"date"=>date('Y-m-d'),"status"=>2));
			
			
			
			$permission=$obj->SelectAllByVal2("dostums_post_permission","user_id",$input_by,"date",date('Y-m-d'),"post_permission");
		
			if($permission=="")
			{
				$new_permission=1;	
			}
			else
			{
				$new_permission=$permission;
			}
			
			$obj->insert("dostums_post_permission_record",array("user_id"=>$input_by,"post_id"=>$post_id,"permission_id"=>$new_permission,"date"=>date('Y-m-d'),"status"=>1));
	$get_country_ids=$obj->FlyQuery("SELECT country_id FROM dostums_post_location_record WHERE user_id='".$input_by."' AND date='".date('Y-m-d')."' ORDER by id DESC LIMIT 1");
			$get_country_id=$get_country_ids[0]->country_id;
			
			$obj->insert("dostums_post_location_record",array("user_id"=>$input_by,"post_id"=>$post_id,"country_id"=>$get_country_id,"date"=>date('Y-m-d'),"status"=>1));
			
			echo $profile_photo;
		}
		elseif($st==7)
		{
			$profile_photo=$imagelib->upload_imageFromString("135","135",UPLOAD_DIR,$file,"page_profile",$data);
			$obj->insert("dostums_photo",array("photo"=>$profile_photo,"photo2"=>$image_name,"status"=>1));
			$photo_ids=$obj->SelectAllByVal2("dostums_photo","photo",$profile_photo,"photo2",$image_name,"id");
			$obj->insert("dostums_post", array("user_id" =>$input_by,"post" =>"  Changed Profile Photo ","photo_id" =>$photo_ids, "post_time" => date('Y-m-d H:i:s'),"date" =>date('Y-m-d'),"status" =>60));
			$post_id=$obj->SelectAllByVal("dostums_post","photo_id",$photo_ids,"id");
			$obj->updateUsingMultiple("dostums_page_profile_photo",array("status"=>1),array("user_id"=>$input_by,"page_id"=>$page_id));
			$obj->insert("dostums_page_profile_photo",array("user_id"=>$input_by,"page_id"=>$page_id,"photo_id"=>$photo_ids,"date"=>date('Y-m-d'),"status"=>2));
			
			$permission=$obj->SelectAllByVal2("dostums_post_permission","user_id",$input_by,"date",date('Y-m-d'),"post_permission");
		
			if($permission=="")
			{
				$new_permission=1;	
			}
			else
			{
				$new_permission=$permission;
			}
			
			$obj->insert("dostums_post_permission_record",array("user_id"=>$input_by,"post_id"=>$post_id,"permission_id"=>$new_permission,"date"=>date('Y-m-d'),"status"=>1));
			$get_country_ids=$obj->FlyQuery("SELECT country_id FROM dostums_post_location_record WHERE user_id='".$input_by."' AND date='".date('Y-m-d')."' ORDER by id DESC LIMIT 1");
			$get_country_id=$get_country_ids[0]->country_id;
			
			$obj->insert("dostums_post_location_record",array("user_id"=>$input_by,"post_id"=>$post_id,"country_id"=>$get_country_id,"date"=>date('Y-m-d'),"status"=>1));
			
			
			echo 1;
			//$obj->insert("notification", array("uid" =>$input_by,"message" =>" Change Profile Photo ","link_id" =>$post_id,"date" =>date('Y-m-d'),"status" =>24));
		}
		elseif($st==8)
		{
			$profile_photo=$imagelib->upload_imageFromString("960","315",UPLOAD_DIR,$file,"page_cover",$data);
			$photo_ids=$obj->insertAndReturnID("dostums_photo",array("photo"=>$profile_photo,"photo2"=>$image_name,"status"=>1));
			$post_id=$obj->insertAndReturnID("dostums_post", array("user_id" =>$input_by,"post" =>"  Changed Cover Photo ","photo_id" =>$photo_ids, "post_time" => date('Y-m-d H:i:s'),"date" =>date('Y-m-d'),"status" =>61));
			$obj->updateUsingMultiple("dostums_page_cover_photo",array("status"=>1),array("user_id"=>$input_by,"page_id"=>$page_id));
			$obj->insert("dostums_page_cover_photo",array("user_id"=>$input_by,"page_id"=>$page_id,"photo_id"=>$photo_ids,"date"=>date('Y-m-d'),"status"=>2));
			
			
			
			$permission=$obj->SelectAllByVal2("dostums_post_permission","user_id",$input_by,"date",date('Y-m-d'),"post_permission");
		
			if($permission=="")
			{
				$new_permission=1;	
			}
			else
			{
				$new_permission=$permission;
			}
			
			$obj->insert("dostums_post_permission_record",array("user_id"=>$input_by,"post_id"=>$post_id,"permission_id"=>$new_permission,"date"=>date('Y-m-d'),"status"=>1));
	$get_country_ids=$obj->FlyQuery("SELECT country_id FROM dostums_post_location_record WHERE user_id='".$input_by."' AND date='".date('Y-m-d')."' ORDER by id DESC LIMIT 1");
			$get_country_id=$get_country_ids[0]->country_id;
			
			$obj->insert("dostums_post_location_record",array("user_id"=>$input_by,"post_id"=>$post_id,"country_id"=>$get_country_id,"date"=>date('Y-m-d'),"status"=>1));
			
			echo $profile_photo;
		}
		else
		{
			unlink($file);
		}
	}
	else
	{
		echo 0;
	}
	
		
	
	
?>	