<?php
include('class/auth.php');
if (isset($_GET['user_id'])) {
    if ($_GET['user_id'] == $input_by) {
        $new_user_id = $input_by;
    } else {
        $new_user_id = $_GET['user_id'];
    }
} else {
    $new_user_id = $input_by;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Dostums - Home </title>
        <?php include('plugin/header_script.php'); ?>
    </head>
    <body class="home">
        <header>
            <div class="header-wrapper">

                <div class="header-nav">
                    <?php include('plugin/header_nav.php'); ?>
                </div>
            </div>
        </header>


        <?php
        //chat box script
        include('plugin/chat_box.php');
//chat box script 
        ?>

        <?php
        //chat user list
        include('plugin/chat_box_head_list.php');
//chat user list 
        ?>

        <div class="main-container page-container section-padd">
            <script type="text/javascript">
                function HidePhotoFtml(photo_id, rowphid)
                {
                    $.post('lib/status.php', {'st': 26, 'photo_id': photo_id}, function (data) {
                        if (data == 1)
                        {
                            $('#friendslistsingle_' + rowphid).hide('fast');
                        }

                    });
                }
                function DeletePhotoFtml(photo_id, rowpdid)
                {
                    $.post('lib/status.php', {'st': 27, 'photo_id': photo_id}, function (data) {
                        if (data == 1)
                        {
                            $('#friendslistsingle_' + rowpdid).hide('fast');
                        }
                        else
                        {
                            alert('Something Went Wrong!!! Please Try Again.')
                        }

                    });
                }

            </script>
            <div class="container-content">
                <div class="container">
                    <!--            <div class="row">
                                    <div class="col-sm-12"><h4 class="pull-left page-title"><i class="mdi-action-perm-media"></i> Photos
                                        <span class="sub-text"> 
                    <?php
                    if ($new_user_id == $input_by) {
                        echo "Your All Photos.";
                    } else {
                        echo "See " . $obj->SelectAllByVal("dostums_user_view", "id", $new_user_id, "name") . " Photos.";
                    }
                    ?>  
                                        </span></h4>
                                        <ol class="breadcrumb pull-right">
                                            <li><a href="#">Profile</a></li>
                                            <li><a href="#">Setting</a></li>
                                            <li class="active">Privacy</li>
                                        </ol>
                                    </div>
                                </div>-->
                    <div class="row">
                        <!--                <div class="col-lg-3 col-md-3 ">
                        
                                            <div class="panel panel-default">
                                                <div class="panel-body p-0">
                                                    <div class="list-group mail-list">
                                                        <a href="#" class="list-group-item no-border active"><i
                                                            class="mdi-action-settings-applications"></i>General </a>
                                                        <a href="#" class="list-group-item no-border"><i
                                                            class="mdi-hardware-security"></i>Security </a>
                                                        <hr class="lihr">
                                                        <a href="#" class="list-group-item no-border"><i class="mdi-content-block"></i>Blocking
                                                        </a> <a href="#" class="list-group-item no-border"><i
                                                                class="mdi-social-notifications"></i>Notifications </a>
                                                        <a href="#"     class="list-group-item no-border"><i
                                                                class="mdi-social-people"></i>Followers
                                                            <b>(354)</b></a>
                                                        <hr class="lihr">
                                                        <a href="#"     class="list-group-item no-border"><i
                                                                class="mdi-action-assignment"></i>Ads
                                                            <b>(354)</b></a>
                        
                                                        <a href="#"     class="list-group-item no-border"><i
                                                                class="mdi-action-payment"></i>Payments
                                                           </a>
                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                        <div class="col-lg-12 col-md-12">

                            <div class="panel ">
                                <div class="panel-heading">
                                    <h4 class="pull-left"> <i class="mdi-action-perm-media"></i> Photos </h4>


                                    <!--                                <div class="panel-tools pull-right">
                                    
                                                                        <div class="panel-search hide" style="display: block;">
                                                                            <input type="text" class="search-input" placeholder="Start typing..." >
                                                                            <i class="search-close">×</i>
                                                                        </div>
                                    <?php //if($new_user_id==$input_by){  ?>
                                                                        <ul class="panel-actions actions">
                                                                            <li>
                                                                                <a class="panel-search-trigger" href="">
                                                                                    <i class="mdi-action-search"></i>
                                                                                </a>
                                                                            </li>
                                    
                                                                            <li>
                                                                                <a href="">
                                                                                    <i class="mdi-content-add"></i> Create Album
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="">
                                                                                    <i class="mdi-image-photo"></i> Add Photos
                                                                                </a>
                                                                            </li>
                                    
                                    
                                    
                                                                            <li class="dropdown">
                                                                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                                                                    <i class="mdi-content-sort"></i> Sort by
                                                                                </a>
                                    
                                                                                <ul class="dropdown-menu dropdown-menu-right">
                                                                                    <li>
                                                                                        <a href="">Last Modified</a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a href="">Last Edited</a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a href="">Name</a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a href="">Date</a>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                    
                                                                            <li class="dropdown">
                                                                                <a aria-expanded="false" data-toggle="dropdown" href="">
                                                                                    <i class="mdi-editor-mode-edit"></i>  Manage
                                                                                </a>
                                    
                                                                                <ul class="dropdown-menu dropdown-menu-right">
                                                                                    <li>
                                                                                        <a href="">Refresh</a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a href="">Listview Settings</a>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                    
                                    <?php //}  ?>
                                    
                                    
                                    
                                                                    </div>    <div style="clear: both"></div>-->
                                    <div style="clear: both"></div>
                                </div>
                                <div class="panel-body">



                                    <div id="photo-content" style="clear:both;" class="row">

                                        <div class="col-lg-12">


                                            <ul id="photo-grid" class="photo-grid list-unstyled no-padding col-lg-12">
                                                <?php
                                                $sqlphoto = $obj->FlyQuery("SELECT b.id,b.photo,(SELECT id from `dostums_post` as e where e.photo_id=b.id AND e.user_id=" . $new_user_id . " LIMIT 1) as post_id from dostums_photo as b WHERE  b.id in (SELECT c.photo_id from dostums_cover_photo as c where c.user_id=" . $new_user_id . ") or b.id in  (SELECT d.photo_id from dostums_profile_photo as d where d.user_id=" . $new_user_id . ")");

                                                //var_dump($sqlphoto);

                                                if (!empty($sqlphoto))
                                                    foreach ($sqlphoto as $photo):
                                                        ?>
                                                        <li>

                                                            <div class="image-action">


                                                                <div class="dropdown dropdown-right-alight">
                                                                    <span data-toggle="dropdown" type="button" class="dropdown-toggle mini-btn" aria-expanded="false">
                                                                        <span class="mdi-action-settings"></span>
                                                                    </span>
                                                                    <ul role="menu" class="dropdown-menu">
                                                                        <li role="presentation"><a href="#" tabindex="-1" role="menuitem">Edit Title </a>
                                                                        </li>
                                                                        <li role="presentation"><a href="#" tabindex="-1" role="menuitem">Add Location</a>
                                                                        </li>
                                                                        <!-- <li role="presentation"><a href="#" tabindex="-1" role="menuitem">Change Date</a> </li>
                                                                        <li role="presentation"><a href="#" tabindex="-1" role="menuitem">Turn off notifications</a></li>-->
                                                                        <li class="divider" role="presentation"></li>
                                                                        <li role="presentation"><button onclick="HidePhotoFtml('<?php echo $photo->photo_id; ?>', '<?php echo $photo->id; ?>')" type="button" class="btn btn-xs" href="#" tabindex="-1" role="menuitem">Hide from
                                                                                Timeline</button></li>
                                                                        <li role="presentation"><button onclick="DeletePhotoFtml('<?php echo $photo->photo_id; ?>', '<?php echo $photo->id; ?>')" type="button" class="btn btn-xs" href="#" tabindex="-1" role="menuitem">Delete This Photo
                                                                            </button></li>
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                            <div class="image-info">
                                                                <div class="share-panel">
                                                                    <a title="#" href="#" class="sp-like"><i class="fa fa-thumbs-up"></i> 26
                                                                        likes</a>
                                                                    <a title="#" href="#" class="sp-comments"><i class="fa fa-comments"></i> 14
                                                                        comments</a>
                                                                </div>
                                                            </div>



                                                            <div class="image-box open-demo-modal" id="<?php echo $photo->post_id; ?>">  
                                                                <img  style="width:204px; height:367px;" src="./profile/<?php echo $photo->photo; ?>" > 
                                                            </div>



                                                        </li>
                                                        <!-- End of grid blocks -->
                                                    <?php endforeach; ?>

                                            </ul>


                                        </div>




                                    </div>

                                    <div style="clear:both;"></div>


                                </div>

                                <!--<div class="panel-footer">
    
                                    <nav>
                                        <ul class="pagination">
                                            <li>
                                                <a href="#" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li>
                                                <a href="#" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
    
                                </div>-->


                            </div>




                        </div>
                    </div>
                </div>
            </div>





            <?php include('plugin/fotter.php'); ?>


            <!-- Latest compiled and minified JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
            <script src="assets/material/dropdownjs/jquery.dropdown.js"></script>
            <script src="assets/material/js/ripples.min.js"></script>
            <script src="assets/material/js/material.min.js"></script>


            <script src="assets/js/jquery.scrollto.js"></script>
            <script src="assets/js/jquery.easing.1.3.js"></script>
            <script src="assets/js/jquery.sticky.js"></script>
            <script src="assets/js/wow.min.js"></script>
            <script src="assets/js/script.js"></script>

            <script src="assets/js/chat.js"></script>

            <script src="lib/setting.js"></script>


            <!-- Include the imagesLoaded plug-in -->
            <script src="assets/plugins/wookmark-jquery-master/imagesloaded.pkgd.min.js"></script>

            <!-- Include the plug-in -->
            <script src="assets/plugins/wookmark-jquery-master/wookmark.min.js"></script>

            <!-- Once the page is loaded, initalize the plug-in. -->
            <script type="text/javascript">

                                                                            var gbks = gbks || {};

                                                                            gbks.jQueryPlugin = function () {

                                                                                this.init = function () {
                                                                                    $(window).resize($.proxy(this.resize, this));
                                                                                    this.resize();
                                                                                };

                                                                                this.resize = function () {
                                                                                    $('.photo-grid').wookmark({
                                                                                        autoResize: true, // This will auto-update the layout when the browser window is resized.
                                                                                        offset: 0, // Optional, the distance between grid items
                                                                                        // outerOffset: 5, // Optional, the distance to the containers border
                                                                                        // itemWidth: 200 // Optional, the width of a grid item
                                                                                        //  offset: 2,
                                                                                        container: $('#photo-content')
                                                                                    });
                                                                                };

                                                                            }

                                                                            var instance = null;
                                                                            $(document).ready(function () {
                                                                                instance = new gbks.jQueryPlugin();
                                                                                instance.init();
                                                                            });


            </script>




    </body>
</html>