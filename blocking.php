<?php
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Dostums - Home </title>
        <?php include('plugin/header_script.php'); ?>

    <body>

        <header>
            <div class="header-wrapper">

                <div class="header-nav">
                    <?php include('plugin/header_nav.php'); ?>
                </div>
            </div>
        </header>


        <?php
        //chat box script
        include('plugin/chat_box.php');
//chat box script 
        ?>

        <?php
        //chat user list
        include('plugin/chat_box_head_list.php');
//chat user list 
        ?>

        <div class="main-container page-container section-padd">
            <div class="mailbox-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12"><h4 class="pull-left page-title"><i class="mdi-action-settings"></i> settings
                                <span class="sub-text"> Manage My Profile  </span></h4>
                            <ol class="breadcrumb pull-right">
                                <li><a href="#">Profile</a></li>
                                <li><a href="setting.php">Setting</a></li>
                                <li class="active">Privacy</li>
                            </ol>
                        </div>
                    </div>
                    <div class="row" style="overflow: hidden;">
                        <div class="col-lg-3 col-md-3 ">

                            <div class="panel panel-default">
                                <div class="panel-body p-0">
                                    <div class="list-group mail-list">
                                        <a href="setting.php" class="list-group-item no-border active"><i
                                                class="mdi-action-settings-applications"></i>General </a>
                                        <a href="security_settings.php" class="list-group-item no-border"><i
                                                class="mdi-hardware-security"></i>Security </a>
                                        <hr class="lihr">
                                        <a href="privacy.php" class="list-group-item no-border active"><i
                                                class="mdi-action-settings-applications"></i>Privacy </a>
                                        <a href="blocking.php" class="list-group-item no-border"><i class="mdi-content-block"></i>Blocking
                                        </a> <a href="notifications.php" class="list-group-item no-border"><i
                                                class="mdi-social-notifications"></i>Notifications </a>
                                        <a href="mobile.php" class="list-group-item no-border"><i
                                                class="mdi-social-notifications"></i>Mobile </a>
                                        <a href="followers.php"     class="list-group-item no-border"><i
                                                class="mdi-social-people"></i>Followers
                                            <b>(354)</b></a>
                                        <hr class="lihr">
                                        <a href="#"     class="list-group-item no-border"><i
                                                class="mdi-action-assignment"></i>Ads
                                            <b>(354)</b></a>

                                        <a href="#"     class="list-group-item no-border"><i
                                                class="mdi-action-payment"></i>Payments
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-9">

                            <div class="ibox ">
                                <div class="ibox-title">
                                    <h5><strong>Manage Blocking</strong></h5>
                                    <div class="ibox-tools">
                                        <button class="def_button" id="basic_info" type="button">
                                            <i class="fa fa-wrench"></i>
                                        </button>
                                    </div>
                                </div>
                                <!--Restricted Lis start-->
                                <div class="ibox-content">

                                    <form class="">
                                        <fieldset>
                                            <div class="row form-group" id="Posts_panel">
                                                <div class="col-sm-12">
                                                    <div class="col-md-12"  style=" background:#F5F5F5; border: 1px #0cc solid; padding: 5px; height:77px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Restricted List</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 ">
                                                                    <div class="col-md-9"style="padding-left:10px;">When you add friends to your Restricted list they can only see the information and posts that you make public.
                                                                        Dostums does not notify your friends when you add them to your Restricted list</div>
                                                                    <div class="col-md-3">    

                                                                        <button type="button"  data-toggle="modal" data-target="#myModal" type="button">Edit List</button>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!--Restricted List end-->



                                            <!-- Modal edit start-->
                                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:60px; ">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header"style="background-color:#99CA3C;">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel" >Edit Restricted</h4>

                                                        </div>

                                                        <div class="modal-body">
                                                            <!-- Single button -->
                                                            <div class="col-md-12">
                                                                <div class="col-md-3 pull-left">
                                                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"style="padding-left:22px;">
                                                                        Action <span class="caret"></span>
                                                                    </button>

                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#">On This list</a></li>
                                                                        <li><a href="#">Friends</a></li>

                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-5 pull-right" style="margin-top: 10px;">
                                                                    <input type="text" class="form-control" id="exampleInputSearch" placeholder="Search">
                                                                </div>   
                                                            </div>    
                                                        </div>
                                                        <div class="modal-footer" style="clear: both; margin-top: 10px;">

                                                            <button type="button" class="btn btn-primary">Finish</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- Modal edit end-->







                                            <!--Block Users start-->
                                            <div class="row form-group" id="Posts_panel">
                                                <div class="col-sm-12">
                                                    <div class="col-md-12"  style=" background:#F5F5F5; border: 1px #0cc solid; padding: 5px; height:200px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Block Users</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 ">
                                                                    <div class="col-md-10"style="padding-left:10px;">

                                                                        Once you block someone, that person can no longer see things you post on your timeline, 
                                                                        tag you, invite you to events or groups, start a conversation with you, or add you as a friend.
                                                                        Note: Does not include apps, games or groups you both participate in.
                                                                    </div>



                                                                </div>

                                                                <div class="col-lg-12">
                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName2"style="padding-left:13px;">Block users</label>
                                                                            <input type="text" class="form-control" id="exampleInputName2"   size="30" placeholder="Add name Or Email" style="background-color:#fff;">
                                                                        </div>

                                                                        <a type="submit" class="btn btn-info">Block</a>
                                                                    </div>
                                                                </div>



                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <!--Block Users end-->










                                            <!--Block messages start-->
                                            <div class="row form-group" id="Posts_panel">
                                                <div class="col-sm-12">
                                                    <div class="col-md-12"  style=" background:#F5F5F5; border: 1px #0cc solid; padding: 5px; height:142px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Block messages</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 ">
                                                                    <div class="col-md-10"style="padding-left:10px;">

                                                                        If you block messages and video calls from someone here, 
                                                                        they won't be able to contact you in the Messenger app either.
                                                                        Unless you block someone's profile, they may be able to post on your Timeline,
                                                                        tag you, and comment on your posts or comments.<a href="3" style="color:#2C99CE;"> Learn more.</a>
                                                                    </div>


                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName2" style="padding-left:9px;">Block messages from</label>
                                                                            <input type="text" class="form-control" id="exampleInputName2" placeholder="Type the name of a friend"   size="30" style="background-color:#fff;">
                                                                        </div>

                                                                        <a type="submit" class="btn btn-info">Block</a>
                                                                    </div>
                                                                </div>



                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <!--Block messages end-->



                                            <!--Block app invites start-->
                                            <div class="row form-group" id="Posts_panel">
                                                <div class="col-sm-12">
                                                    <div class="col-md-12"  style=" background:#F5F5F5; border: 1px #0cc solid; padding: 5px; height:142px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Block app invites</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 ">
                                                                    <div class="col-md-10"style="padding-left:10px;">

                                                                        Once you block app invites from someone, you'll automatically ignore future app requests from that friend.
                                                                        To block invites from a specific friend, click the "Ignore All Invites From This Friend" link under your latest request.
                                                                    </div>



                                                                </div>
                                                                <div class="col-md-12">

                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName2" style="padding-left:12px;">Block app invites</label>
                                                                            <input type="text" class="form-control" id="exampleInputName2" placeholder="Type the name of a friend"   size="50" style="background-color:#fff;" >
                                                                        </div>


                                                                    </div>
                                                                </div>



                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <!--Block app invites end-->



                                            <!--Block event invites start-->
                                            <div class="row form-group" id="Posts_panel">
                                                <div class="col-sm-12">
                                                    <div class="col-md-12"  style=" background:#F5F5F5; border: 1px #0cc solid; padding: 5px; height:142px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Block event invites</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12">
                                                                    <div class="col-md-10"style="padding-left:10px;">

                                                                        Once you block event invites from someone, you'll automatically ignore future event requests from that friend.
                                                                    </div>



                                                                </div>
                                                                <div class="col-md-12">

                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName2"style="padding-left:10px;">Block event invites</label>
                                                                            <input type="text" class="form-control"   size="48" id="exampleInputName2" placeholder="Type the name of a friend" style="background-color:#fff;">
                                                                        </div>


                                                                    </div>
                                                                </div>


                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <!--Block event invites end-->



                                            <!--Block apps start-->
                                            <div class="row form-group" id="Posts_panel">
                                                <div class="col-sm-12">
                                                    <div class="col-md-12"  style=" background:#F5F5F5; border: 1px #0cc solid; padding: 5px; height:142px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Block apps</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 ">
                                                                    <div class="col-md-10"style="padding-left:10px;">

                                                                        Once you block an app, it can no longer contact you or get non-public information about you through Facebook.<a style="color:#2C99CE;"> Learn more.</a>
                                                                    </div>



                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName2"style="padding-left:10px;">Block apps</label>
                                                                            <input type="text" class="form-control" id="exampleInputName2" placeholder="Type the name of a friend"  size="55" style="background-color:#fff;">
                                                                        </div>


                                                                    </div>
                                                                </div>


                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <!--Block apps end-->




                                            <!--Block event invites start-->
                                            <div class="row form-group" id="Posts_panel">
                                                <div class="col-sm-12">
                                                    <div class="col-md-12"  style=" background:#F5F5F5; border: 1px #0cc solid; padding: 5px; height:142px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Block Pages</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 "style="padding-left:22px;">


                                                                    Once you block a Page, that Page can no longer interact with your posts or like or reply to your comments. 
                                                                    You'll be unable to post to the Page's Timeline or message the Page.
                                                                    If you currently like the Page, blocking it will also unlike and unfollow it.



                                                                </div>
                                                                <div class="col-md-12">

                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName2"style="padding-left:8px;">Block Pages</label>
                                                                            <input type="text" class="form-control" id="exampleInputName2" size="54" placeholder="Type the name of a friend" style="background-color:#fff; margin-top: 10px;">
                                                                        </div>


                                                                    </div>
                                                                </div>


                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <!--Block event invites end-->














                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                        </fieldset>
                                    </form>

                                </div>
                            </div>












                            <?php include('plugin/fotter.php') ?>


                            <script src="assets/material/dropdownjs/jquery.dropdown.js"></script>
                            <script src="assets/material/js/ripples.min.js"></script>
                            <script src="assets/material/js/material.min.js"></script>


                            <script src="assets/js/jquery.scrollto.js"></script>
                            <script src="assets/js/jquery.easing.1.3.js"></script>
                            <script src="assets/js/jquery.sticky.js"></script>
                            <script src="assets/js/wow.min.js"></script>
                            <script src="assets/js/script.js"></script>

                            <script src="assets/js/chat.js"></script>

                            <script src="lib/setting.js"></script>
                            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
                            </body>
                            </html>
