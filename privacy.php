<?php
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Dostums - Home </title>
        <?php include('plugin/header_script.php'); ?>
    </head>
    <body>

        <header>
            <div class="header-wrapper">

                <div class="header-nav">
                    <?php include('plugin/header_nav.php'); ?>
                </div>
            </div>
        </header>


        <?php
        //chat box script
        include('plugin/chat_box.php');
//chat box script 
        ?>

        <?php
        //chat user list
        include('plugin/chat_box_head_list.php');
//chat user list 
        ?>

        <div class="main-container page-container section-padd">
            <div class="mailbox-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12"><h4 class="pull-left page-title"><i class="mdi-action-settings"></i> settings
                                <span class="sub-text"> Manage My Profile  </span></h4>
                            <ol class="breadcrumb pull-right">
                                <li><a href="#">Profile</a></li>
                                <li><a href="setting.php">Setting</a></li>
                                <li class="active">Privacy</li>
                            </ol>
                        </div>
                    </div>
                    <div class="row" style="overflow: hidden;">
                        <div class="col-lg-3 col-md-3 ">

                            <div class="panel panel-default">
                                <div class="panel-body p-0">
                                    <div class="list-group mail-list">
                                        <a href="setting.php" class="list-group-item no-border active"><i
                                                class="mdi-action-settings-applications"></i>General </a>
                                        <a href="security_settings.php" class="list-group-item no-border"><i
                                                class="mdi-hardware-security"></i>Security </a>
                                        <hr class="lihr">
                                        <a href="privacy.php" class="list-group-item no-border active"><i
                                                class="mdi-action-settings-applications"></i>Privacy </a>
                                        <a href="blocking.php" class="list-group-item no-border"><i class="mdi-content-block"></i>Blocking
                                        </a> <a href="notifications.php" class="list-group-item no-border"><i
                                                class="mdi-social-notifications"></i>Notifications </a>
                                        <a href="mobile.php" class="list-group-item no-border"><i
                                                class="mdi-social-notifications"></i>Mobile </a>
                                        <a href="followers.php"     class="list-group-item no-border"><i
                                                class="mdi-social-people"></i>Followers
                                            <b>(354)</b></a>
                                        <hr class="lihr">
                                        <a href="dostums_ads.php"     class="list-group-item no-border"><i
                                                class="mdi-action-assignment"></i>Ads
                                            <b>(354)</b></a>

                                        <a href="#"     class="list-group-item no-border"><i
                                                class="mdi-action-payment"></i>Payments
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-9">

                            <div class="ibox ">
                                <div class="ibox-title">
                                    <h5><strong>Privacy Settings and Tools</strong></h5>
                                    <div class="ibox-tools">
                                        <button class="def_button" id="basic_info" type="button">
                                            <i class="fa fa-wrench"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="ibox-content">

                                    <form class="">
                                        <fieldset>
                                            <div class="row form-group" id="Posts_panel">
                                                <div class="col-sm-12">
                                                    <div class="col-md-12"  style=" background:#F5F5F5; border: 1px #0cc solid; padding: 5px; height:400px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Posts</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 ">
                                                                    <div class="col-md-10"> <strong style="padding-left:10px;"> Who can see your future posts?</strong></div>
                                                                    <div class="col-md-2">    
                                                                        <a href="#" style="color: #2C99CE;">close</a><br>
                                                                    </div>

                                                                </div>
                                                                <div style="padding-left:25px;">You can manage the privacy of things you share by using the audience selector <a href="#" style="color: #2C99CE;">right where you post.</a>
                                                                    This control remembers your selection so future posts will be shared with the same audience unless you change it.</div>



                                                            </div>


                                                            <div class="col-md-12" style="padding-left:226px; margin-top:11px;">

                                                                <div class="form-group col-md-6">

                                                                    <select class="form-control" id="select_city" style=" background-color:#FFFFFF;">
                                                                        <option>Who should see this?</option>
                                                                        <option>Only me</option>
                                                                        <option>Friend</option>
                                                                        <option>Public</option>
                                                                        <option>5</option>
                                                                    </select>
                                                                </div>

                                                                <!--<div class="_tpp" style="border:1px solid #ccc; margin-top:10px 0;"></div>-->
                                                            </div>


                                                            <div class="col-md-12" style="padding-left:228px;margin-top:5px;">
                                                                <a class="col-md-9" href="#">Review all your posts and things your's tagged in</a>
                                                                <a class="col-md-3" style=color:#2C99CE;">Use Activity log</a>
                                                            </div>

                                                            <div class="col-md-12" style="padding-left:228px;margin-top:5px;">
                                                                <a class="col-md-9" href="#">Limit the audience for posts you've shared with friends of friends or Public?  </a>
                                                                <div class="col-md-9"><strong> Limit The Audience for Old Posts on Your Timeline </strong> </div><a class="col-md-3" style=color:#2C99CE;">Limit past post</a>
                                                                <div class="col-md-12">
                                                                    <span class="glyphicon glyphicon-alert col-md-1 " style="color:#ffcc00;"></span>
                                                                    <p class="col-md-11">If you use this tool, content on your timeline you've shared with friends of friends or Public will change to Friends. 
                                                                        Remember: people who are tagged and their friends may see those posts as well.
                                                                        You also have the option to individually change the audience of your posts. 
                                                                        Just go to the post you want to change and choose a different audience.</p>
                                                                    <a style="color:#2C99CE; padding-left:60px;" href="#"> Learn about changing old posts</a>

                                                                </div>
                                                                <div class="col-md-9" href="#" style="padding-left:75px;">
                                                                    <a> <button>Limit Old Posts</button></a>
                                                                </div>


                                                            </div>
                                                        </div>




                                                    </div>





                                                </div>
                                            </div>


                                            <!--Friend Requests start-->

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <div class="col-md-12" style=" background:#F5F5F5; border: 1px #0cc solid; padding:15px;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Friend Requests</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 " id="friend_requests_panel">
                                                                    <div class="col-md-6"style="padding-left:10px;">Who can send you friend requests?</div>
                                                                    <div class="col-md-4" style="padding-left:1px;">mmmmmmm</div>
                                                                    <div class="col-md-2">    
                                                                        <a href="#" style="color: #2C99CE;"> Edit</a><br>
                                                                    </div>

                                                                </div>

                                                                <!--Friend Requests start hidden-->


                                                                <div class="col-md-12" id="friend_requests_panel2"style=" background:#fff;  padding-left:5px; margin-top:8px; display: none;" >

                                                                    <span class="text-right col-md-3" style="position: absolute;" id="friend_requests_panel2_close"><i class="fa fa-close"></i></span>

                                                                    <div class="col-md-9">


                                                                        <div class="col-md-12">

                                                                        </div>

                                                                        <div class="col-md-12"style="padding-right:60px;padding-left:60px;margin-top:8px;">
                                                                            <div class="form-group">
                                                                                <strong class="text-center">Who can send you friend requests?</strong> 
                                                                                <select class="form-control" id="select_city" style=" background:#f5f5f5; margin-top:11px;">
                                                                                    <option>Everyone</option>
                                                                                    <option>Friend to Friend</option>


                                                                                </select>
                                                                            </div>

                                                                        </div>



                                                                    </div>


                                                                </div>


                                                            </div>
                                                            <!--Friend Requests hidden end-->

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <script>
                                                $('document').ready(function () {
                                                    $('#friend_requests_panel, #friend_requests_panel2_close').click(function () {
                                                        $('#friend_requests_panel2').toggle('show');

                                                    });
                                                });

                                            </script>

                                            <!--Friend Requests end-->


                                            <!--Contact Info start -->
                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <div class="col-md-12" style=" background:#F5F5F5; border: 1px #0cc solid;">
                                                        <div class="control-group">
                                                            <div class="col-md-3">
                                                                <label class="control-label" for="textinput"><strong>Contact Info</strong></label>
                                                            </div>
                                                            <div class="col-md-9">

                                                                <div class="col-md-12 "style="margin-top:6px;"  id="contact_info_panel">
                                                                    <div class="col-md-6">Who can look you up using the email address you provided?</div>
                                                                    <div class="col-md-4">ffffffff</div>
                                                                    <div class="col-md-2">    
                                                                        <a href="#" style="color: #2C99CE;padding-left:10px;"> Edit</a><br>
                                                                    </div>

                                                                </div>

                                                                <!--Contact Info start hidden-->


                                                                <div class="col-md-12" id="contact_info_panel2" style=" background: #fff; padding: 5px; display: none; height:100px;margin-top:8px;" >

                                                                    <span class="text-right col-md-1" style="position: absolute; display: block; right: 10px;" id="contact_info_panel2_close"><i class="fa fa-close"></i></span>
                                                                    <div class="col-md-12"style="padding-right:60px;padding-left:60px;margin-top:8px;">
                                                                        <div class="form-group" style="padding-left:2px;">
                                                                            <strong>Who can look you up using the email address you provided?</strong>
                                                                            <select class="form-control" id="select_city" style="background:#F5F5F5; width: 200px; margin-top:11px;">
                                                                                <option>Everyone</option>
                                                                                <option>Friend to Friend</option>


                                                                            </select>
                                                                        </div>

                                                                    </div>

                                                                </div>



                                                                <script>
                                                                    $('document').ready(function () {
                                                                        $('#contact_info_panel, #contact_info_panel2_close').click(function () {
                                                                            $('#contact_info_panel2').toggle('show');

                                                                        });
                                                                    });

                                                                </script>

                                                                <!--Contact Info hidden end-->

                                                                <!--Contact Info phone start-->
                                                                <div class="col-md-12"  style=" background:#F5F5F5;">

                                                                    <div class="col-md-12" id="contact_info_phone">
                                                                        <div class="col-md-6" style="margin-left: 0px; padding-left: 0px;">Who can look you up using the phone number you provided? </div>
                                                                        <div class="col-md-4">ffffffff</div>
                                                                        <div class="col-md-2">    
                                                                            <a href="#" style="color: #2C99CE;padding-left:20px;"> Edit</a><br>
                                                                        </div>
                                                                    </div>

                                                                    <!--Contact Info 2 start hidden-->

                                                                    <div class="col-md-12" id="contact_info_phone_panel" style=" background: #fff; padding: 5px; display: none; height:100px;margin-top:8px;" >

                                                                        <span class="text-right col-md-1" style="position: absolute; display: block; right: 10px;" id="contact_info_phone_close2"><i class="fa fa-close"></i></span>
                                                                        <div class="col-md-12"style="padding-right:60px;padding-left:60px;margin-top:8px;">
                                                                            <div class="form-group" style="padding-left:2px;">
                                                                                <strong>Who can look you up using the phone number you provided?</strong>
                                                                                <select class="form-control" id="select_friend" style="background:#F5F5F5; width: 200px; margin-top:11px;">
                                                                                    <option>Everyone</option>
                                                                                    <option>Friend to Friend</option>


                                                                                </select>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div> 

                                                                <script>
                                                                    $('document').ready(function () {
                                                                        $('#contact_info_phone, #contact_info_phone_close2').click(function () {
                                                                            $('#contact_info_phone_panel').toggle('show');

                                                                        });
                                                                    });

                                                                </script>

                                                                <!--Contact Info 2 hidden end-->
                                                                <!--Contact Info phone end-->





                                                                <!--  Contact Info last start-->
                                                                <div class="row form-group">
                                                                    <div class="col-md-12">
                                                                        <div id="contact_last_panel" class="col-md-12">
                                                                            <div class="col-md-6"> Do you want search engines outside of Dostums to link to your profile?
                                                                            </div>
                                                                            <div class="col-md-4">ffffffff</div>
                                                                            <div class="col-md-2">    
                                                                                <a href="#" style="color: #2C99CE; padding-left: 10px;"> Edit</a><br>
                                                                            </div>
                                                                        </div>
                                                                        <!--  Contact Info last 2 hidden start-->

                                                                        <div class="col-md-12" id="contact_last_part3" style=" background:#fff;  padding: 5px; height:224px;margin-top:15px;">

                                                                            <div class="control-group">

                                                                                <div class="col-md-12">

                                                                                    <div class="col-md-12">
                                                                                        <span class="text-right col-md-1" style="position: absolute; display: block; right: 10px;" id="contact_info_last_close2"><i class="fa fa-close"></i></span>
                                                                                        <div class="col-md-12" style="margin-top: 10px;"><strong>Do you want search engines outside of Dostums to link to your profile?</strong></div>

                                                                                        <div class="col-md-12">
                                                                                            <div class="form-group">

                                                                                                <ul style="margin-top: 10px;">
                                                                                                    <li>When this setting is on, search engines may link to your profile in their results.</li>
                                                                                                    <li>When this setting is off, search engines will stop linking to your profile, but this may take some time. 
                                                                                                        Your profile can still be found on Facebook if people search for your name.</li>
                                                                                                </ul>

                                                                                            </div>
                                                                                            <div class="checkbox">
                                                                                                <label>
                                                                                                    <input type="checkbox"> Allow search engines outside of Dostums to link to your profile
                                                                                                </label>
                                                                                            </div>

                                                                                        </div>


                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                    </div>



                                                                    <!--  Contact Info last 2 hidden end-->




                                                                    <script>
                                                                        $('document').ready(function () {
                                                                            $('#contact_last_panel,#contact_info_last_close2').click(function () {
                                                                                $('#contact_last_part3').toggle('show');

                                                                            });
                                                                        });

                                                                    </script>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </fieldset>
                                                    </form>

                                                </div>
                                            </div>












                                            <?php include('plugin/fotter.php') ?>


                                            <script src="assets/material/dropdownjs/jquery.dropdown.js"></script>
                                            <script src="assets/material/js/ripples.min.js"></script>
                                            <script src="assets/material/js/material.min.js"></script>


                                            <script src="assets/js/jquery.scrollto.js"></script>
                                            <script src="assets/js/jquery.easing.1.3.js"></script>
                                            <script src="assets/js/jquery.sticky.js"></script>
                                            <script src="assets/js/wow.min.js"></script>
                                            <script src="assets/js/script.js"></script>

                                            <script src="assets/js/chat.js"></script>

                                            <script src="lib/setting.js"></script>

                                            </body>
                                            </html>
