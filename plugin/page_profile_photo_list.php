<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title"><i class="fa fa-image"></i> Recent Photos <a class="pull-right" href="photos.php?user_id=<?php echo $new_user_id; ?>">
            <small> View all</small>
        </a></div>

    </div>

    <div class="panel-body panel-gallery">
        <div class="has-gallery">

            <?php 
			$sqlphoto=$obj->FlyQuery("SELECT b.id,b.photo,(SELECT id from `dostums_post` as e where e.photo_id=b.id AND e.user_id=".$new_user_id." LIMIT 1) as post_id from dostums_photo as b WHERE  b.id in (SELECT c.photo_id from dostums_cover_photo as c where c.user_id=".$new_user_id.") or b.id in  (SELECT d.photo_id from dostums_profile_photo as d where d.user_id=".$new_user_id.")");			
			if(!empty($sqlphoto))
			foreach($sqlphoto as $photo):
			?>
            <a class="open-demo-modal"  id="<?php echo $photo->post_id; ?>"> 
            <img src="./profile/<?php echo $photo->photo; ?>" alt="img"> 
            </a>
			<?php endforeach; ?>

        </div>
	</div>
</div>