<style>
.panel-search{ left:15px !important; width:95% !important; height:55px !important;}
.list-group .list-group-item{padding:10px 15px !important; border:1px solid #DDDDDD !important;}
.btn-default{border:1px solid #DDDDDD !important; }
</style>
<div class="panel ">
    <div class="panel-heading">
         <?php
			 $stringQuery = "SELECT dg.name,dg.group_id,dgi.class FROM dostums_group as dg Left JOIN dostums_group_icon as dgi on dg.icon_id = dgi.id WHERE dg.user_id ='".$input_by."' and dg.name != '' or dg.name is null Order BY dg.name DESC  ";
			$lstGroups = $obj->FlyQuery($stringQuery); 
			?>


        <div class="panel-tools pull-left">

            <div class="panel-search hide" style="display: block;">
                <input id="group-search" type="text" class="search-input" placeholder="Search All Groups..." >
                <i class="search-close">×</i>
            </div>
            <script>
				$('document').ready(function(e) {
						$('#search-result-feed').hide()
						$('#group-search').keyup(function(e) {
							$('#default-feed').hide();
							var countvalues=$(this).val().length;
							if(countvalues>=4)
							{
								$.post("./lib/search_groups.php", {'st': 1,'search_gr_data': $(this).val()},function(fetch){
									var datacl=jQuery.parseJSON(fetch);
									var opt3=datacl.data;
									$('#default-feed').hide();
									$('#search-result-feed').show();
									$('#search-result-feed').html(opt3);
								});
								
							}
							else if(countvalues==0)
							{
								$('#search-result-feed').hide();
								$('#default-feed').show();
							}
							else
							{
								
							}
						});
						
						/*$('.friends').click(function(e){
							var getlink=$(this).find('a').attr('href');
							alert('success');	
						});*/
					
				});
				
				
			</script>
            

            <ul class="panel-actions actions pull-left"  style="padding-left:0px !important; padding-right:0px !important;">
                <li>
                    <a href="">
                        <i class="fa fa-refresh text-primary"></i> Suggested Groups <span class="badge badge-info">20</span>
                    </a>
                </li>
                
                <li>
                    <a href="">
                        <i class="fa fa-plus-circle text-primary"></i> New Groups <span class="badge badge-primary">20</span>
                    </a>
                </li>
                
                <li>
                    <a href="">
                        <i class="fa fa-check-square-o text-primary"></i> Your Groups
                        <span class="badge badge-success">
                        <?php 
						echo is_array($lstGroups) ? (count($lstGroups) >=1 ? count($lstGroups): "&nbsp;0") : "&nbsp;0"; 
						?>
                        </span>
                    </a>
                </li>
                
                <li>
                    <a class="panel-search-trigger" href="">
                        <i class="mdi-action-search"></i>
                    </a>
                </li>
            </ul>





        </div>    <div style="clear: both"></div>
    </div>
    <div class="panel-body">



        <div id="" style="clear:both;" class="row">

            <div class="col-lg-12">
            	<div id="search-result-feed" class="list-group"></div>
				<div id="default-feed" class="list-group">
                		<a class="list-group-item bg-default">
                        <h5><strong><i class="fa fa-sitemap fa-2x" style="color:#4CAF50; margin-right:15px;"></i>Groups You Manage&nbsp;:
                        &nbsp; Total &nbsp; 
						<span class="badge badge-success">
						<?php 
						echo is_array($lstGroups) ? (count($lstGroups) >=1 ? count($lstGroups): "&nbsp;0") : "&nbsp;0"; 
						?></span>
                        </strong></h5></a>
                  <?php
						 if(is_array($lstGroups)){
						 if(count($lstGroups)>0){	  
						 //$i =0;
						  
						 foreach($lstGroups as $lVal){  
						?>
						<div class="list-group-item">
                        <a href="group.php?group_id=<?php echo $lVal->group_id; ?>">
						<span style="color:#2C99CE; margin-right:15px; font-size:24px;" class="<?php echo   $lVal->class == "" ? "fa fa-users text-warning": $lVal->class;?>">
						</span><span style="font-size:14px;"><?php echo $lVal->name; ?></span></a>
                        <button class="btn btn-success btn-xs pull-right"><i class="fa fa-eye">&nbsp; &nbsp;</i>View Group</button>
                        </div>
                        
						<?php 
							 //if($i == 15) break;
							 //$i++;
						 }
						 
						}
						
					}?>
                </div>
            </div>


        </div>

        <div style="clear:both;"></div>


    </div>

    <div class="panel-footer">

        <!--<nav>
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>-->

    </div>


</div>