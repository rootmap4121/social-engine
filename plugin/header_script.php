<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon" sizes="57x57" href="images/icon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/icon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/icon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/icon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/icon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/icon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/icon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/icon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/icon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="images/icon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/icon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/icon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/icon/favicon-16x16.png">
<link rel="manifest" href="images/icon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="images/icon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">


<!-- Bootstrap -->
<link href="assets/css/bootstrap.css" rel="stylesheet">

<!--Jasny Bootstrap -->
<link href="../test_plugins/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
<script src="../test_plugins/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>


<!-- Include roboto.css to use the Roboto web font, material.css to include the theme and ripples.css to style the ripple effect -->
<link href="assets/material/css/roboto.min.css" rel="stylesheet">
<link href="assets/material/css/material.min.css" rel="stylesheet">
<link href="assets/material/css/ripples.min.css" rel="stylesheet">
<link href="assets/material/dropdownjs/jquery.dropdown.css" rel="stylesheet">


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="lib/js/jquery-1.10.2.min.js"></script>

<?php if ($obj->filename() != "photos.php") { ?>
    <!-- jQuery (necessary for newsbox news slider) -->
    <script src="assets/plugins/newsbox/jquery.bootstrap.newsbox.min.js"></script>
<?php } ?>


<!-- jQuery (custom scroll plugins) -->
<script src="http://cdn.jsdelivr.net/jquery.mcustomscrollbar/3.0.6/jquery.mCustomScrollbar.concat.min.js"></script>
<link href="http://cdn.jsdelivr.net/jquery.mcustomscrollbar/3.0.6/jquery.mCustomScrollbar.min.css" rel="stylesheet">
<?php
if (in_array($obj->filename(), array("profile.php", "profiles.php"))) {
    ?>
    <!--jQuery (fullcalendar  plugins) -->
    <script src="http://fullcalendar.io/js/fullcalendar-2.4.0/lib/moment.min.js"></script>
    <script src="http://fullcalendar.io/js/fullcalendar-2.4.0/fullcalendar.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.4.0/fullcalendar.min.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.4.0/fullcalendar.print.css" rel="stylesheet">
<?php } ?>

<link href="assets/plugins/modal-library/mikes-modal.css" rel="stylesheet">
<script src="assets/plugins/modal-library/mikes-modal.min.js" type="text/javascript"></script>
<script>
<?php
if (!in_array($obj->filename(), array("signup.php","login.php"))) {
//if ($obj->filename() != "signup.php" || $obj->filename() != "login.php") { 
    ?>
        $(document).ready(function (e) {
            $.cookie("windowchat1", "");
            $.cookie("windowchat2", "");
            $.cookie("windowchat3", "");
            $.cookie("chat_window_1_user", "0");
            $.cookie("chat_window_2_user", "0");
            $.cookie("chat_window_3_user", "0");
        });

        jQuery(function () {
            $('#totalfriendrequest').html("..");
            window.setInterval(function () {
                load_frnd_notification = {'st': 1};
                $.post('lib/shout.php', load_frnd_notification, function (data_notification) {
                    var frndData = jQuery.parseJSON(data_notification);
                    var friend_request = frndData.friend_request;
                    $('#totalfriendrequest').html(friend_request);

                });
            }, 1000);

            window.setInterval(function () {
                $.post('lib/chat.php', {'st': 4}, function (data_notification) {
                    $('#message_notification').html(data_notification);

                });
            }, 2000);


            $("#totalfriendrequest").click(function (e) {
                load_frnd_request_detail = {'st': 2};
                var def_html = "<li><a href='friend-requests.php'><div class='dropdown-messages-box'><div class='media-body '><strong>Loading Please Wait...</strong></div></div></a></li>";
                $('#totalfriendrequestbox').html(def_html);

                $.post('lib/shout.php', load_frnd_request_detail, function (data_notification) {
                    $('#totalfriendrequestbox').html(data_notification);
                });
            });



            $(".open-demo-modal").click(function (e) {
                e.preventDefault();
                $('#the-lights').show();
                $("#myModal").mikesModal();
                //$("#myModal").css("height","500px");
                var getID = $(this).attr('id');
                //alert(getID);

                load_modal_post = {'st': 4, 'image_post': getID};
                $.post('lib/imageload.php', load_modal_post, function (data) {
                    //$("#myModal").html(data);
                    var datacl = jQuery.parseJSON(data);
                    var thumb = datacl.thumb;
                    var bigphoto = datacl.bigphoto;
                    var user_id = datacl.user_id;
                    var user_name = datacl.user_name;
                    var user_photo = datacl.user_photo;
                    var user_bigphoto = datacl.user_bigphoto;
                    var like = datacl.like;
                    var comment = datacl.comment;
                    var permission = datacl.permission;
                    var post_time = datacl.post_time;
                    var post_content = datacl.post_content;
                    var post_id = datacl.post_id;

                    $('#modal_big_image').attr("src", "./profile/" + bigphoto);
                    $('#modal_user_image').attr("src", "./profile/" + user_photo);
                    $('#modal_user_link').attr("href", "profile.php?user_id=" + user_id);
                    $('#modal_user_link').html(user_name);
                    $('#modal_post_permission').html(permission);
                    $('#modal_post_time').html(post_time);
                    $('input[name=mikepostid]').val(post_id);
                    $('textarea[name=make-textarea]').attr("id", "miketextarea_" + post_id);
                    if (like != 0)
                    {
                        $('.sp-mike-like').html('<i class="fa fa-thumbs-up"></i> <span class="mike-like">' + like + ' </span> <span id="mike-like-place">Like</span>');
                    }
                    else
                    {
                        $('.sp-mike-like').html('<i class="fa fa-thumbs-up"></i> <span class="mike-like"></span> <span id="mike-like-place">Like</span>');
                    }

                    if (comment != 0)
                    {
                        $('.sp-mike-comments').html('<i class="fa fa-comments"></i> <span class="mike-comment">' + comment + ' </span> <span id="mike-comment-place">Comments</span>');
                    }
                    else
                    {
                        $('.sp-mike-comments').html('<i class="fa fa-comments"></i> <span id="mike-comment-place">Comments</span>');
                    }

                    $('.sp-mike-like').attr('name', post_id);
                    $('#modal_post_content').html(post_content);
                    $('.mcc_modal').attr("id", "modal_mcc" + getID);
                    $('.commentList').attr("id", "modal_comment_list_instant_load_" + getID);
                    var getBasic = $('#modal_likecommentbox').attr('class');
                    $('.' + getBasic).attr("id", "modal_loadallcomment" + getID);
                    $('.modal_postcomment').attr("id", "modal_postcomment" + getID);
                    //var post_id=getID;
                    //load allcomment and like
                    
                    loadcommentprofile(post_id);
                    loadlikecombarprofile(post_id);

                    
                    //load all comment and like

                });
            });

        });

        function loadcommentprofile(post_id)
        {
            load_new_comment = {'st': 1, 'post_id': post_id};
            $.post('lib/comment.php', load_new_comment, function (comment) {
                if (comment)
                {
                    $('#modal_comment_list_instant_load_' + post_id).fadeIn('slow');
                    $('#modal_comment_list_instant_load_' + post_id).html(comment);
                }
                else
                {
                    window.location.refresh();
                }
            });
        }
        
        function loadlikecombarprofile(post_id)
        {
            load_count_comment = {'st': 3, 'post_id': post_id};
                    $.post('lib/comment.php', load_count_comment, function (commentc) {

                        var globaldataconds = false;
                        var datacl = jQuery.parseJSON(commentc);
                        var like = datacl.likes;
                        var commentd = datacl.comment;
                        var globalcomlik = (like - 0) + (commentd - 0);
                        if (globalcomlik != 0)
                        {
                            globaldataconds = true;
                        }

                        if (globaldataconds)
                        {

                            if (globalcomlik != 0)
                            {

                                
                                $('#modal_loadallcomment' + post_id).css('display', 'inline-block');
                                $('#modal_mcc' + post_id).fadeIn('slow');
                                if (like == 0)
                                {
                                    $('#modal_mcc' + post_id).html(commentd + " comments");
                                }
                                else if (commentd == 0)
                                {
                                    $('#modal_mcc' + post_id).html(like + " likes");
                                }
                                else if (like != 0 && commentd != 0)
                                {
                                    
                                    $('#modal_mcc' + post_id).html(like + " likes and " + commentd + " comments");

                                }
                                else
                                {
                                    $('#modal_loadallcomment' + post_id).css('display', 'none');
                                    $('#modal_mcc' + post_id).fadeIn('slow');
                                }

                                if (commentd != 0)
                                {
                                    $('#modal_postcomment' + post_id).html(commentd);
                                }
                                else
                                {
                                    $('#modal_postcomment' + post_id).html(" ");
                                }


                            }
                            else
                            {
                                $('#modal_loadallcomment' + post_id).fadeOut('slow');
                                $('#modal_loadallcomment' + post_id).css('display', 'none');
                            }

                        }
                        else
                        {
                            //location.reload();
                            $('#modal_loadallcomment' + post_id).fadeOut('slow');
                            $('#modal_loadallcomment' + post_id).css('display', 'none');
                        }



                    });
        }

<?php } ?>

</script>

<!-- main stylesheet -->
<link href="assets/css/style.css" rel="stylesheet">
<!--New Iconic fort added by shanto for better iconic view-->
<link href="./assets/foundation-icons/foundation-icons.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="assets/js/custom.modernizr.js"></script>
<script>
    paceOptions = {
        elements: true
    };
</script>

<!-- ajax library-->
<script src="lib/script.js"></script>
<!--ajax library-->
<?php if ($obj->filename() == "signup.php") { ?>
    <link rel="stylesheet" href="lib/js/jquery-ui.css">
    <script src="lib/js/jquery-1.10.2.js"></script>
    <script src="lib/js/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'});
        });
    </script>
    <?php
}

if (in_array($obj->filename(), array("profile.php", "profiles.php", "home.php", "group.php", "page.php"))) {
    ?>

    <style type="text/css">
        .fileUpload {
            position: relative !important;
            overflow: hidden !important;
            margin: 0px !important;
        }
        .fileUpload input.upload {
            position: absolute !important;
            top: 0 !important;
            right: 0 !important;
            margin: 0 !important;
            padding: 0 !important;
            font-size: 20px !important;
            cursor: pointer !important;
            opacity: 0 !important; 
            filter: alpha(opacity=0) !important;
        }
    </style>
    <link rel="stylesheet" href="assets/css/crop/style.css" type="text/css" />
    <script src="lib/cropbox.js"></script>
<?php } ?>
<style type="text/css">
    .def_button{ border:0px; background:none; color:#2C99CE; }
    .def_button2{border-radius: 5px !important; background:none; color:#2C99CE; min-width: 30px !important; padding-left: 0px !important; padding-right: 0px !important; }
    .def_button2:hover{background-color: #ff5722 !important; color: #ffffff !important;}

    @media (max-width: 435px) { 
        .panel-customs-post.panel-status .panel-heading .post-types li.active::before{
            bottom: -43px !important;
        }

        .panel-customs-post.panel-status .panel-heading .post-types li.active::after{
            bottom: -41px !important;
        }
        .panel-customs-post .panel-footer .btn{
            /*padding-left: 10px !important;
            padding-right: 10px !important;*/
            float:left !important;
        }
    }
</style>
