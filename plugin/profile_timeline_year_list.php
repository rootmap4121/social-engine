<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title pull-left"><i class="mdi-device-access-time"></i> Timeline</h3>
        <div class="btn-group pull-right btn-vrt boxHeaderOptions">
                <i class="mdi-navigation-refresh"></i>
            <ul class="dropdown-menu">
                <li><a title="#" href="#"><i class="mdi-device-access-time"></i> Time span</a></li>
                <li><a title="#" href="#"><i class="mdi-communication-chat"></i> Chart type</a></li>
                <li><a title="#" href="#"><i class="mdi-navigation-refresh"></i> Refresh</a></li>
            </ul>
        </div>
        <div style="clear: both"></div>
    </div>
    <div class="panel-body has-timeline">
        <ul style="max-width: 300px;" class="nav nav-pills nav-stacked">
            <li class="active"><a href="javascript:void(0)">2015</a></li>
            <li class=""><a href="javascript:void(0)">2014</a></li>
            <li class=""><a href="javascript:void(0)">2013</a></li>
            <li class=""><a href="javascript:void(0)">2012</a></li>
            <li class=""><a href="javascript:void(0)">2011</a></li>
            <li class=""><a href="javascript:void(0)">2010</a></li>
        </ul>
    </div>
</div>