<input type="hidden" id="donate_user_donation_type" value="0" />
<input type="hidden" id="donate_user_first_name" value="" />
<input type="hidden" id="donate_user_last_name" value="" />
<input type="hidden" id="donate_user_address" value="" />
<input type="hidden" id="donate_user_City_Town" value="" />
<input type="hidden" id="donate_select_city" value="" />
<input type="hidden" id="donate_Preferred_Email_Address" value="" />
<input type="hidden" id="donate_Personal" value="" />
<input type="hidden" id="donate_Phone_Number" value="" />
<input type="hidden" id="donate_select_Home" value="" />
<input type="hidden" id="donate_NameOnCard" value="" />
<input type="hidden" id="donate_CardNumber" value="" />
<input type="hidden" id="donate_select_month" value="" />
<input type="hidden" id="donate_select_year" value="" />
<input type="hidden" id="donate_Group_Type" value="" />
<input type="hidden" id="donate_Group_Or_Business_Name" value="" />
<input type="hidden" id="donate_Search" value="" />
<input type="hidden" id="donate_why_donation" value="" />
<input type="hidden" id="donate_select_money_symbol" value=""/>
<input type="hidden" id="donate_amount_pay" value=""/>

<script>
    $(document).ready(function () {

        $('#step-2').hide('slow');
        $('#step-3').hide('slow');
        $('#step-4').hide('slow');
        $('#step-5').hide('slow');

        $('#step-2-btn').click(function () {
            $('#step-1').hide('slow');
            $('#step-2').show('slow');

            var whyneeded = $("#why_donation").val();
            $("#donate_why_donation").val(whyneeded);

            var currency = $("#select_money_symbol").val();
            $("#donate_select_money_symbol").val(currency);

            var amount = $("#exampleInputmoney").val();
            $("#donate_amount_pay").val(amount);


            var first_name = $("#exampleFirstName").val();
            $("#donate_user_first_name").val(first_name);

            var last_name = $("#exampleLastName").val();
            $("#donate_user_last_name").val(last_name);

            var address = $("#exampleStreetAddress").val();
            $("#donate_user_address").val(address);

            var City_Town = $("#exampleCity_Town").val();
            $("#donate_user_City_Town").val(City_Town);

            var Preferred_select_city = $("#select_city").val();
            $("#donate_select_city").val(Preferred_select_city);



        });


        $('#step-3-btn').click(function () {
            $('#step-1').hide('slow');
            $('#step-2').hide('slow');
            $('#step-3').show('slow');

            var Preferred_Email = $("#PreferredEmailAddress").val();
            $("#donate_Preferred_Email_Address").val(Preferred_Email);

            var Preferred_Personal = $("#Personal").val();
            $("#donate_Personal").val(Preferred_Personal);

            var PhoneNumber = $("#Phone_Number").val();
            $("#donate_Phone_Number").val(PhoneNumber);

            var selectHome = $("#select_Home").val();
            $("#donate_select_Home").val(selectHome);

        });

        $('#step-4-btn').click(function () {

            var Preferred_NameOnCard = $("#NameOnCard").val();
            $("#donate_NameOnCard").val(Preferred_NameOnCard);

            var Preferred_CardNumber = $("#CardNumber").val();
            $("#donate_CardNumber").val(Preferred_CardNumber);

            var Preferred_select_month = $("#select_month").val();
            $("#donate_select_month").val(Preferred_select_month);

            var Preferred_select_year = $("#select_year").val();
            $("#donate_select_year").val(Preferred_select_year);
            
            save();
            
        });

        $('#step-5-btn').click(function () {
            $('#step-1').hide('slow');
            $('#step-2').show('slow');
            $('#step-3').hide('slow');
            $('#step-4').hide('slow');
            $('#step-5').hide('slow');

            var Group_Type = $("#GroupType").val();
            $("#donate_Group_Type").val(Group_Type);

            var Group_Or_Business_Name = $("#GroupOrBusinessName").val();
            $("#donate_Group_Or_Business_Name").val(Group_Or_Business_Name);


        });

        $("#step_5_finish").click(function () {
            var Search_submit = $("#Search").val();
            $("#donate_Search").val(Search_submit);

            $('#step-1').hide('slow');
            $('#step-2').show('slow');
            $('#step-3').hide('slow');
            $('#step-4').hide('slow');
            $('#step-5').hide('slow');
            


        });



        $('#btn-Previous1').click(function () {
            $('#step-2').hide('slow');
            $('#step-3').hide('slow');
            $('#step-4').hide('slow');
            $('#step-5').hide('slow');
            $('#step-1').show('slow');
            $('#step_id_start').show('slow');
            checkboxcheck();
            console.log(1);
        });
        
        $('#btn-Previous2').click(function () {
            $('#step-3').hide('slow');
            $('#step-2').show('slow');
            checkboxcheck();
        });
        
        $('#btn-Previous3').click(function () {
            $('#step-4').hide('slow');
            $('#step-3').show('slow');
            checkboxcheck();
        });
        
        



        //radio button validation

        //radio button validation

    });

    function checkboxcheck()
    {
        if (document.getElementById('donate_user_step_1').checked)
        {
            $('#step_id_start').show();
            $('#step-5').hide();
            var st1 = $("#donate_user_step_1").val();
            $("#donate_user_donation_type").val(st1);
        }
        else if (document.getElementById('donate_user_step_4').checked)
        {
            $("#step_id_start").hide();
            $('#step-5').hide();
            $('#step-4').show();
            var st1 = $("#donate_user_step_4").val();
            $("#donate_user_donation_type").val(st1);
        }
        else if (document.getElementById('donate_user_step_5').checked)
        {
            $("#step_id_start").hide();
            $('#step-4').hide();
            $('#step-5').show();
            var st1 = $("#donate_user_step_5").val();
            $("#donate_user_donation_type").val(st1);
        }
     
    }


    function save()
    {
            var first_name = $("#donate_user_first_name").val();
            var last_name = $("#donate_user_last_name").val();
            var address = $("#donate_user_address").val();
            var City_Town = $("#donate_user_City_Town").val();
            var select_city = $("#donate_select_city").val();
            var Email_Address = $("#donate_Preferred_Email_Address").val();
            var Personal = $("#donate_Personal").val();
            var Phone_Number = $("#donate_Phone_Number").val();
            var select_Home = $("#donate_select_Home").val();
            var NameOnCard = $("#donate_NameOnCard").val();
            var CardNumber = $("#donate_CardNumber").val();
            var month = $("#donate_select_month").val();
            var year = $("#donate_select_year").val();
            var Group_Type = $("#donate_Group_Type").val();
            var Group_Or_Business_Name = $("#donate_Group_Or_Business_Name").val();
            var Search = $("#donate_Search").val();
            var disabled_Select = $("#donate_why_donation").val();
            var Select_money_symbol = $("#select_money_symbol").val();
            var amount_pay = $("#Inputmoney_amnt").val();
 
            $.post("./lib/donate.php", {'st': 1, 'first_name': first_name, 'last_name': last_name, 'address': address, 'City_Town': City_Town,
                'useselect_cityr': select_city, 'Email_Address': Email_Address, 'Personal': Personal, 'Phone_Number': Phone_Number,
                'select_Home': select_Home, 'NameOnCard': NameOnCard, 'CardNumber': CardNumber, 'month': month, 'year': year,
                'Group_Type': Group_Type, 'Group_Or_Business_Name': Group_Or_Business_Name, 'Search': Search,
                'why_donation': disabled_Select, ' Select_money_symbol': Select_money_symbol, 'amount_pay': amount_pay},function (data) {
                //alert(data);
                 $("#labelfordonationmsg").html("<label>Donation Record Successfully Saved</label>");
                  $("#myModaldif").modal('hide');
            });
            
            $("#myModaldif").modal('hide');
            
    }


</script>

<!-- Modal -->
<div class="modal fade" id="myModaldif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:50px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-left">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Donate for Dostums: <span id="labelfordonationmsg"></span></h4>
            </div>
            <div class="modal-body">


                <!--Step-01 starts here-->

                <fieldset id="step-1">


                    <div class="form-group text-center">
                        <label>Primary Contact Person&nbsp (Step-1/3):</label>
                    </div>

                    <div class="col-sm-12">
                        <label class="col-md-4">
                            <input name="radios" onclick="checkboxcheck()" id="donate_user_step_1" value="1"    type="radio">
                            individual
                        </label>
                        <label class="col-md-4">
                            <input name="radios" onclick="checkboxcheck()" id="donate_user_step_4" value="2"  type="radio">
                            group's
                        </label>
                        <label class="col-md-4">
                            <input name="radios" onclick="checkboxcheck()" id="donate_user_step_5" value="3"  type="radio">
                            multiple  person
                        </label>
                    </div>

                    <div id="show_place">
                        <div class="first_page" id="step_id_start" style="clear: both; display: none;">

                            <div class="form-group">

                                <label for="exampleFirstName">First Name</label>
                                <input type="text" class="form-control" id="exampleFirstName" placeholder="First Name">
                            </div>
                            <div class="form-group">
                                <label for="exampleLastName">Last Name</label>
                                <input type="text" class="form-control" id="exampleLastName" placeholder="Last Name">
                            </div>

                            <div class="form-group">
                                <label for="exampleStreetAddress">Street Address</label>
                                <textarea name="address" class="form-control" id="exampleStreetAddress" placeholder="Address"></textarea>

                            </div>
                            <div class="form-group">
                                <label for="exampleCity_Town">City/Town</label>
                                <input type="text" class="form-control" id="exampleCity_Town" placeholder="City/Town">
                            </div>

                            <div class="form-group">
                                <label for="exampleCity/Town">City/Town</label>
                                <select class="form-control" id="select_city">
                                    <option>Dhaka</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <button id="step-2-btn" type="button" class="btn btn-success">Next</button>
                            </div>
                        </div>  


                        <div id="step-4" style="display: none;">

                            <div class="second_page" style="clear:both;">
                                <div class="col-lg-12" style="margin-left:0px; padding-left:0px;">

                                    <div class="form-group">
                                        <label for="GroupType" class="control-label">Group Type</label>


                                        <select class="form-control" id="GroupType">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="GroupOrBusinessName">Group Or Business Name</label>
                                        <input type="text" class="form-control" id="GroupOrBusinessName" placeholder="Group Or Business Name">
                                    </div>
                                    <div class="form-group">

                                        <button type="button" class="btn btn-primary" id="btn-Previous3">Previous</button>
                                        <button id="step-5-btn" type="button" class="btn btn-success">Next</button>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div id="step-5" style="display: none;">
                            <div class="form-group col-lg-12">
                                <label class="col-md-8">Multiple's Person</label>
                                <input type="text" class="form-control" placeholder="Search" id="Search">
                                <button type="button" class="btn btn-primary" id="btn-Previous4">Previous</button>
                                <button type="submit" id="step_5_finish" class="btn btn-success">Next</button>

                            </div>

                        </div> 
                    </div>   




                </fieldset>
                <!--Step-01 ends here-->

                <!--Step-02 starts here-->
                <fieldset id="step-2" style="display: none;">
                    <div class="form-group">
                        <label for="PreferredEmailAddress">Preferred Email Address</label>
                        <input type="text" class="form-control" id="PreferredEmailAddress" placeholder="example - jane.doe@compassion.com">
                    </div>
                    <div class="form-group" >
                        <select class="form-control" id="Personal">
                            <option>Personal</option>
                            <option>Business</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <label for="">Phone Number</label>
                        <input type="text" class="form-control" id="Phone_Number" placeholder="example - 555-555-5555">
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="select_Home">
                            <option>Home</option>
                            <option>Business</option>
                            <option>Mobile</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary" id="btn-Previous1">Previous</button>
                        <button id="step-3-btn" type="button" class="btn btn-success">Next</button>
                    </div>
                </fieldset>

                <!--Step-02 ends here-->

                <!--Step-03 start-->
                <fieldset id="step-3" style="display: none;">
                    <label>My Billing Information:</label>
                    <label>I prefer to pay the total amount charged today ($50.00) with:</label>

                    <div class="col-lg-12">

                        <label class="col-md-12">
                            <input type="radio" name="optionsRadios" id="optionsRadios4" value="option4" checked>
                            Credit or Debit Card (You may discontinue your sponsorship at any time)
                        </label>
                    </div>
                    <div class="first_page" style="clear: both;">

                        <div class="form-group">
                            <label for="NameOnCard">Name on Card</label>
                            <input type="text" class="form-control" id="NameOnCard" placeholder="Required field">
                        </div>
                        <div class="form-group">
                            <label for="CardNumber">Card Number</label>
                            <input type="text" class="form-control" id="CardNumber" placeholder="Card Number">
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-xs-3">
                                <label for="PhoneNumber">Expiration Date</label>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6 col-xs-3">
                                <select class="form-control" id="select_month">
                                    <option>01-january</option>
                                    <option>02</option>
                                    <option>03</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-xs-3">
                                <select class="form-control" id="select_year">
                                    <option>2015</option>
                                    <option>2016</option>
                                    <option>2017</option>
                                    <option>2018</option>
                                    <option>2019</option>
                                    <option>2000</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary btn-previous" id="btn-Previous2"">Previous</button>
                            <button id="step-4-btn" type="button" class="btn btn-success">Finish & Save Donation Info</button>
                        </div>
                    </div>

                </fieldset>
                <!--Step-03 end-->


                <!--step-05 start-->

                <!--step-05 end--> 
            </div>
        </div>
    </div>
</div>
