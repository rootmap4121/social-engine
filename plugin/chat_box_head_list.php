<aside id="chat" style="margin-top:25px;">
    <ul role="tablist" class="tab-nav tn-justified" style="overflow: hidden;" tabindex="0">
        <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="friends" href="#friends"
                                                  aria-expanded="true">Friends</a></li>
        <li role="presentation" ><a data-toggle="tab" role="tab" aria-controls="online" href="#online" aria-expanded="false">Online Now</a></li>
    </ul>

    <div class="chat-search">
        <div class="fg-line">
            <input type="text" placeholder="Search People" class="form-control" kl_virtual_keyboard_secure_input="on">
        </div>
    </div>

    <div class="tab-content tab-content-chat">
        <div id="friends" class="tab-pane fade active in" role="tabpanel">
            <div class="listview chatListView">
                <?php
                $chknotification = $obj->FlyQuery("SELECT alldata.* FROM (select a.id, 
                a.uid, 
                a.status, 
                IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
                concat(du.first_name,' ',du.last_name) as name
                FROM 
                dostums_friend as a 
                LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=a.uid AND dpp.status='2' LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id 
                LEFT JOIN dostums_user as du ON du.id=a.uid
                WHERE 
                a.to_uid='" . $new_user_id . "' 
                AND a.status='2') as alldata WHERE alldata.uid!='" . $new_user_id . "'");
                if (!empty($chknotification))
                    foreach ($chknotification as $noti) {
                        ?>
                        <!-- START list group item-->
                        <a href="#" class="lv-item" id="chat_window" name="<?php echo $noti->uid; ?>" datalink="profile.php?user_id=<?php echo $noti->uid; ?>">
                            <div class="media">
                                <div class="pull-left p-relative">
                                    <img alt="" src="./profile/<?php echo $noti->photo; ?>" class="lv-img-sm">
                                    <i class="chat-status-busy"></i>
                                </div>
                                <div class="media-body">
                                    <div class="lv-title"><?php echo $noti->name; ?></div>
                                    <small class="lv-small">Available</small>
                                </div>
                            </div>
                        </a>
                        <!-- END list group item-->
                        <?php
                    }
                ?>
            </div>
        </div>
        <div id="online" class="tab-pane fade" role="tabpanel">
            <div class="listview chatListView">
                
                <?php
                $chknotification = $obj->FlyQuery("SELECT alldata.* FROM (select a.id, 
                a.uid, 
                a.status, 
                IFNULL(dp.photo,'generic-man-profile.jpg') as photo,
                concat(du.first_name,' ',du.last_name) as name
                FROM 
                dostums_friend as a 
                LEFT JOIN dostums_profile_photo as dpp ON dpp.user_id=a.uid AND dpp.status='2' LEFT JOIN dostums_photo as dp ON dp.id=dpp.photo_id 
                LEFT JOIN dostums_user as du ON du.id=a.uid
                WHERE 
                a.to_uid='" . $new_user_id . "' 
                AND a.status='2') as alldata WHERE alldata.uid!='" . $new_user_id . "'");
                if (!empty($chknotification))
                    foreach ($chknotification as $noti) {
                        ?>
                        <!-- START list group item-->
                        <a href="#" class="lv-item" id="chat_window" name="<?php echo $noti->uid; ?>" datalink="profile.php?user_id=<?php echo $noti->uid; ?>">
                            <div class="media">
                                <div class="pull-left p-relative">
                                    <img alt="" src="./profile/<?php echo $noti->photo; ?>" class="lv-img-sm">
                                    <i class="chat-status-busy"></i>
                                </div>
                                <div class="media-body">
                                    <div class="lv-title"><?php echo $noti->name; ?></div>
                                    <small class="lv-small">Available</small>
                                </div>
                            </div>
                        </a>
                        <!-- END list group item-->
                        <?php
                    }
                ?>
                
            </div>
        </div>
    </div>

</aside>