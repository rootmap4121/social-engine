<?php 
$sqlcurrent_profile_image=$obj->SelectAllByVal2("dostums_profile_photo","user_id",$new_user_id,"status",2,"photo_id");
if(!empty($sqlcurrent_profile_image))
{
	$profile_image="./profile/".$obj->SelectAllByVal("dostums_photo","id",$sqlcurrent_profile_image,"photo");	
}
else
{
	$profile_image="./images/user/generic-man-profile.jpg";
}

$sqlcurrent_cover_image=$obj->SelectAllByVal2("dostums_cover_photo","user_id",$new_user_id,"status",2,"photo_id");
if(!empty($sqlcurrent_cover_image))
{
	$cover_image="./profile/".$obj->SelectAllByVal("dostums_photo","id",$sqlcurrent_cover_image,"photo");	
}
else
{
	$cover_image="./images/user/ocean-beach-generic-cover.jpg";
}
?>
<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
    <div class="cover-photo" id="cover-photo" style="
    background:url('<?php echo $cover_image; ?>'); background-color: #435e9c; background-repeat: no-repeat;
background-position: center; background-size: cover;">
		<?php if($obj->filename()=="profile.php"){  ?>
        <div class="fileUpload" style="float:left;">
            <span id="uploadFile"></span>
            <input type="file" id="file_cover" class="upload" />
        </div>    
            
        <a href="#" class="change-cover-photo"> <i class="fa fa-image"></i> change photo</a>
		<?php } ?>
        <div class="profile-photo-box">
                    <?php if($obj->filename()=="profile.php"){  ?>
                    <div class="fileUpload" style="float:left;">
                        <span id="uploadFile"></span>
                        <input type="file" id="file" class="upload" />
                    </div>	
                    
                    <?php } ?>
            <img id="profile_photo" class="profile-photo show-in-modal" src="<?php echo $profile_image; ?>">
            <?php if($obj->filename()=="profile.php"){  ?>
            <a href="#" class="change-profile-photo"> <i class="fa fa-image"></i> change photo</a>
            <?php } ?>
        </div>

        <div class="cover-name"><?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"name"); ?> <br>
            <span class="user-name-display">@<?php echo $obj->SelectAllByVal("dostums_user","id",$new_user_id,"first_name")."-".$new_user_id; ?></span>
        </div>

        <div class="profile-photo-user">
			
            <ul class="list-unstyled list-inline  navbar-global navbar-right">
            	<?php 
				if($obj->filename()!="profile.php")
				{ 
					$chkfrnd=$obj->exists_multiple("dostums_friend_view",array("uid"=>$input_by,"to_uid"=>$new_user_id));
					if($chkfrnd==0)
					{ 
						?>
						<li><button  onclick="frndrequest_Profile('<?php echo $new_user_id; ?>','profile_request_status_<?php echo $new_user_id; ?>','1')"  id="profile_request_status_<?php echo $new_user_id; ?>" class="user-actions-follow-button btn-stroke btn-primary btn-sm  follow-button btn">
							<i class="fa fa-user-plus"></i> add friend
						</button></li>	
						<li class="dropdown ">
							<a data-toggle="dropdown"
							   class="dropdown-toggle btn-primary btn-stroke btn-sm btn-stroke" href="#"
							   aria-expanded="false"> <span class="fa fa-gear "></span></a>
							<ul class="dropdown-menu dropdown-sm">
								<li><a href="#"><span class="fa fa-user-times"></span> Block <?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"name"); ?> </a></li>
								<li><a href="#"><span class="fa fa-user-secret"></span> Report <?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"name"); ?> </a></li>
							</ul>
						</li>
						<?php 
					}
					else
					{
						$chkfrndstatus=$obj->SelectAllByVal2("dostums_friend_view","uid",$input_by,"to_uid",$new_user_id,"status");
						if($chkfrndstatus==0)
						{
						?>
						<li><button  onclick="frndrequest_Profile('<?php echo $new_user_id; ?>','profile_request_status_<?php echo $new_user_id; ?>','3')"   id="profile_request_status_<?php echo $new_user_id; ?>"   class="user-actions-follow-button btn-stroke btn-primary btn-sm  follow-button btn">
							<i class="fa fa-close"></i> Request Sent
						</button></li>	
						<li class="dropdown ">
							<a data-toggle="dropdown"
							   class="dropdown-toggle btn-primary btn-stroke btn-sm btn-stroke" href="#"
							   aria-expanded="false"> <span class="fa fa-gear "></span></a>
							<ul class="dropdown-menu dropdown-sm">
                                                            <li><button type="button" class="btn btn-xs"><span class="fa fa-user-times"></span> Block <?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"name"); ?> </button></li>
								<li><button type="button" class="btn btn-xs"><span class="fa fa-user-secret"></span> Report <?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"name"); ?> </button></li>
							</ul>
						</li>
						<?php
						}
						elseif($chkfrndstatus==1)
						{
						?>
						<li><button   id="profile_request_status_<?php echo $new_user_id; ?>"  onclick="frndrequest_Profile('<?php echo $new_user_id; ?>','profile_request_status_<?php echo $new_user_id; ?>','2')" class="user-actions-follow-button btn-stroke btn-primary btn-sm  follow-button btn">
							<i class="fa fa-check"></i> Confirm Request
						</button></li>	
						<li class="dropdown ">
							<a data-toggle="dropdown"
							   class="dropdown-toggle btn-primary btn-stroke btn-sm btn-stroke" href="#"
							   aria-expanded="false"> <span class="fa fa-gear "></span></a>
							<ul class="dropdown-menu dropdown-sm">
								<li><a href="#"><span class="fa fa-close"></span> Cancel Request </a></li>
                                <li><a href="#"><span class="fa fa-user-times"></span> Block <?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"name"); ?> </a></li>
								<li><a href="#"><span class="fa fa-user-secret"></span> Report <?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"name"); ?> </a></li>
							</ul>
						</li>
						<?php
						}
						elseif($chkfrndstatus==2)
						{
						?>
						<li><button   id="profile_request_status_<?php echo $new_user_id; ?>"  onclick="frndrequest_Profile('<?php echo $new_user_id; ?>','profile_request_status_<?php echo $new_user_id; ?>','3')" class="user-actions-follow-button btn-stroke btn-primary btn-sm  follow-button btn">
							<i class="fa fa-user"></i> Friend
						</button></li>	
						<li class="dropdown ">
							<a data-toggle="dropdown"
							   class="dropdown-toggle btn-primary btn-stroke btn-sm btn-stroke" href="#"
							   aria-expanded="false"> <span class="fa fa-gear "></span></a>
							<ul class="dropdown-menu dropdown-sm">
                            	<li><a href="#"><span class="fa fa-user-times"></span> Unfriend <?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"name"); ?> </a></li>
								<li><a href="#"><span class="fa fa-user-times"></span> Block <?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"name"); ?> </a></li>
								<li><a href="#"><span class="fa fa-user-secret"></span> Report <?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"name"); ?> </a></li>
							</ul>
						</li>
						<?php
						}
					}
				} 
				?>
                <?php if($obj->filename()=="profile.php"){  ?>
                <li>

                    <a href="setting.php" class="btn-success btn-stroke btn-sm  follow-button btn " style="font-weight:bold !important;">
                        <i class="fa fa-pencil">&nbsp;</i> edit profile
                    </a>
                </li>
                <?php } ?>
            </ul>
            
        </div>
    </div>

    <?php 
    //profile time line bar start
    include('profile_timeline_bar.php');
    //profile time line end
    ?>

</div>