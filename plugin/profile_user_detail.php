<div class="panel panel-default">
    <div class="panel-heading">
        <h5><i class="mdi-action-account-circle"></i> User Detail</h5>
    </div>
    <div class="panel-body">

        <div class="panel-content profile-content">
            <h4><strong><?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"name"); ?>  </strong></h4>

            <p><i class="fa  fa-clock-o "></i> Join <?php echo substr($obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"date"),0,4); ?></p>

            <p><i class="fa fa-map-marker"></i> From <?php echo $obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"city_id"); ?>, <?php echo $obj->SelectAllByVal("dostums_country","id",$obj->SelectAllByVal("dostums_user_view","id",$new_user_id,"country_id"),"country_name"); ?></p>
            <h5>
                <strong>About me </strong>
            </h5>

            <p>
                <?php echo $obj->SelectAllByVal("dostums_user_about","user_id",$new_user_id,"about_short"); ?>
            </p>

            <!--<div class="row profile-info-graph">
                <div class="col-md-4">
                    <i class="fa fa-bullhorn c1  fa-2x"></i>
                    <h5><span><?php 
                    //169
                    //echo $obj->exists_multiple("dostums_post_view",array("user_id"=>$new_user_id));
                     ?></span> Posts</h5>
                </div>
                <div class="col-md-4">
                    <i class="fa fa-users fa-2x c2 "></i>
                    <h5><span>28</span> Following</h5>
                </div>
                <div class="col-md-4">
                    <i class="fa fa-bar-chart-o c3 fa-2x"></i>
                    <h5><span>240</span> Followers</h5>
                </div>
            </div>
            <div class="user-button">
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-primary btn-sm btn-block" type="button"><i
                                class="fa fa-envelope"></i> Message
                        </button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-success btn-sm btn-block" type="button"><i
                                class="fa fa-thumbs-o-up"></i> Follow
                        </button>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</div>