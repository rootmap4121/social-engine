<div class="panel panel-default panel-customs-post ">
    <div class="dropdown">

    <span class="dropdown-toggle" type="button" data-toggle="dropdown">
        <span class="glyphicon glyphicon-chevron-down  "></span>
    </span>
        <ul class="dropdown-menu" role="menu">
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                <div class="post-top-dropdown">
                    <div >Hide post</div>
                    <div class="text-muted">See fewer posts like this</div>
                </div>
            </a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                <div class="post-top-dropdown">
                    <div >Unfollow Regina</div>
                    <div class="text-muted">Stop seeing posts but stay friends</div>
                </div>
            </a></li>
            <li role="presentation" class="divider"></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Turn on
                notifications</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a>
            </li>
        </ul>


    </div>

    <div class="panel-heading">
        <img class="img-circle pull-left  " src="images/user/user3.jpg" alt="user3">

        <h3>Regina Dealova <span class="text-muted ">added a new photo</span></h3>
        <h5><span>Shared publicly</span> - <span> Jun 15, 2016</span></h5>
    </div>
    <div class="panel-body">
        <p>কাভানিকে খোঁচা মারায় ৩ ম্যাচ নিষিদ্ধ হারা : <a href="pinterest.com">http://bangla.bdnews24.com/sport/article989759.bdnews</a>
        </p>

        <div class="panel-customs-post-image"
             href="http://photos.bdnews24.com/media/2015/06/29/jara-1.jpg/ALTERNATES/w620/JARA+1.jpg">
            <!-- 16:9 aspect ratio -->
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item"
                        src="//www.youtube.com/embed/UYBhFprHUuA"></iframe>
            </div>


        </div>
    </div>
    <div class="panel-bottom">
        <div class="panel-footer has-share-panel">

            <div class="row">

                <div class="col-sm-12">

                    <div class="share-panel">
                        <a title="#" href="#" class="sp-like"><i class="fa fa-thumbs-up"></i> 26
                            likes</a>
                        <a title="#" href="#" class="sp-comments"><i class="fa fa-comments"></i>
                            14 comments</a>
                        <a title="#" href="#" class="sp-share"><i class="fa fa-share"></i> 10
                            shares</a>
                    </div>

                </div>

            </div>

        </div>
        <div class="comments-box has-comments">

            <div class="all-comments-count">
                View 9 more comments
            <span class="dropdown-toggle" type="button" data-toggle="dropdown">
           <span class="glyphicon glyphicon-chevron-down"></span>
           </span>
            </div>

            <div class="comments-list-wrapper">
                <ul class="commentList">
                    <li>
                        <div class="commenterImage">
                            <img src="https://lh3.googleusercontent.com/-ll1jsqW_tKI/AAAAAAAAAAI/AAAAAAAAFzk/sxT1miO22bs/s46-c-k-no/photo.jpg">
                        </div>
                        <div class="commentText">
                            <p ><strong> <a href="user-profile.html"> Lamia Alonso </a>
                            </strong> Hello this is a test comment.</p> <span
                                class="date sub-text">on March 5th, 2015</span>

                        </div>
                    </li>
                    <li>
                        <div class="commenterImage">
                            <img src="https://lh3.googleusercontent.com/-A_v3UELYm34/AAAAAAAAAAI/AAAAAAAADOE/APCzKJPAP_8/s46-c-k-no/photo.jpg">
                        </div>
                        <div class="commentText">
                            <p ><strong><a href="user-profile.html">Jaspreet Chahal</a>
                            </strong> Hello this is a test comment and this comment is
                                particularly
                                very long and it goes on and on and on.</p> <span
                                class="date sub-text">on March 5th, 2015</span>

                        </div>
                    </li>
                    <li>
                        <div class="commenterImage">
                            <img src="http://lorempixel.com/50/50/people/9">
                        </div>
                        <div class="commentText">
                            <p ><strong> <a href="user-profile.html">Lisa Doll</a>
                            </strong>
                                Hello this is a test comment.</p> <span class="date sub-text">on March 5th, 2015</span>

                        </div>
                    </li>
                </ul>
            </div>

            <div class="input-placeholder">Add a comment...</div>


        </div>
        <div class="write-comments panel-customs-post-comment">
            <img alt="User Image" src="images/user/default-user-picture.gif" class="img-circle">

            <div class="panel-customs-post-textarea">
                <textarea rows="4"></textarea>
                <button class="btn btn-success disabled " type="submit">Post comment</button>
                <button class="btn  btn-default " type="reset">Cancel</button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>