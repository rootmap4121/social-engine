<script>
    $(document).ready(function () {
        $.post('lib/imageload.php', {'st': 3, 'usrid':<?php echo $input_by; ?>}, function (defim) {
            var ndcl = jQuery.parseJSON(defim);
            var thumbdef = ndcl.thumb;
            $('img[name=comment_profile_photo]').attr('src', './profile/' + thumbdef);
        });
    });
</script>
<?php
include('class/extraClass.php');
$extra = new SiteExtra();
//if ($obj->filename() == "profiles.php" || $new_user_id != $input_by) {
if (in_array($obj->filename(), array("profiles.php","profile.php"))) {    
    $sqlquery = $obj->FlyQuery("SELECT b.id,b.user_id,b.from_user_id,b.photo_id,b.comment,b.likes,b.post,b.post_time,b.post_status,b.status
	FROM dostums_post_view AS b
	WHERE user_id=" . $new_user_id . " OR from_user_id=" . $new_user_id . " OR to_user_id=" . $new_user_id . " ORDER BY id DESC LIMIT 0,14");
} else {
    $sqlquery = $obj->FlyQuery("SELECT b.id,b.user_id,b.from_user_id,b.photo_id,b.comment,b.likes,b.post,b.post_time,b.post_status,b.status
	FROM dostums_post_view AS b
	WHERE EXISTS (SELECT * FROM dostums_friend_view 
	WHERE dostums_friend_view.uid = " . $new_user_id . " AND b.user_id = dostums_friend_view.to_uid) OR user_id=" . $new_user_id . " ORDER BY id DESC LIMIT 0,14");
}

$postbreak=1;
if (!empty($sqlquery)) {
    foreach ($sqlquery as $post):
        $new_post_head_id = $post->id . "statushead" . time();
        @$chkpermission = $obj->SelectAllByID_multiple("dostums_post_permission_record", array("user_id" => $post->user_id, "post_id" => $post->id));
        if (@$chkpermission[0]->permission_id == 3 && @$post->user_id == $input_by) {
            
            //status 3 code
            ?>
<div class="panel panel-default  panel-customs-post">
            <div class="dropdown">
                <span class="dropdown-toggle" type="button" data-toggle="dropdown">
                    <span class=" glyphicon glyphicon-chevron-down "></span>
                </span>
                <ul class="dropdown-menu" role="menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-edit"></i> Edit Post </a>
                    </li>
                    <li role="presentation"><a role="menuitem" class="dostums-post-delete" id="<?php echo $post->id; ?>" tabindex="-1" href="#"><i class="fa fa-trash"></i> Delete Post </a>
                    </li>
                    <!--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Hide from
                        Timeline</a></li>-->
                    <!--/*<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Add Location</a>
                    </li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Change Date</a>
                    </li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Turn off
                        notifications</a></li>
                    <li role="presentation" class="divider"></li>
                    */-->

                </ul>
            </div>

            <div class="panel-heading" id="<?php echo $new_post_head_id; ?>">
                <script type="text/javascript">
                    $(document).ready(function () {
                        load_post = {'st': 1, 'post_id':<?php echo $post->id; ?>};
                        $.post('lib/imageload.php', load_post, function (datapost) {
                            if (datapost != 0)
                            {
                                var datacl = jQuery.parseJSON(datapost);
                                var user_id = datacl.user_id;
                                var name = datacl.name;
                                var thumb = datacl.thumb;
                                var thumbbig = datacl.thumbbig;

                                var to_user_id = datacl.to_user_id;



                                if (to_user_id == 0)
                                {
                                    var sharedhtmlname = "Shared publicly";
                                    var sharedhtml = "Posted";
                                }
                                else
                                {

                                    var to_name = datacl.to_name;
                                    var thumb2 = datacl.thumb2;
                                    var thumbbig2 = datacl.thumbbig2;
                                    var sharedhtmlname = "Shared on <a href='profile.php?user_id=" + to_user_id + "' style='color:#000;'>" + to_name + "</a> Timeline";
                                    var sharedhtml = "Posted";
                                }

                                var datahtml = "<img class='img-circle pull-left' src='./profile/" + thumb + "' alt='" + thumb + "'><h3><a href='profile.php?user_id=" + user_id + "'>" + name + "</a> " + sharedhtmlname + "</h3><h5><span>" + sharedhtml + "</span> - <span><?php echo $extra->duration($post->post_time, date('Y-m-d H:i:s')); ?></span></h5>";

                                $('#<?php echo $new_post_head_id; ?>').html(datahtml);
                            }
                            else
                            {
                                window.location.refresh();
                            }
                        });

                    });
                </script>
            </div>
        <?php include('status/post.php'); ?>
            <div class="panel-bottom">
                <div class="panel-footer has-share-panel">
                    <div class="row">
                        <div class="col-sm-12">
            <?php include('status/postactionbar.php'); ?>
                        </div>
                    </div>
                </div>
                            <?php include('status/comment_list.php'); ?>
                            <?php include('status/comment.php'); ?>
            </div>
        </div>
                <?php
            //status 3 code
                 $postbreak++;
            
        } elseif (@$chkpermission[0]->permission_id == 2 || @$chkpermission[0]->permission_id == 1) {
            //status one and 2 start
            ?>
<div class="panel panel-default  panel-customs-post" style="border:1px #09f solid;">
            <div class="dropdown">
                <span class="dropdown-toggle" type="button" data-toggle="dropdown">
                    <span class=" glyphicon glyphicon-chevron-down "></span>
                </span>
                <ul class="dropdown-menu" role="menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-edit"></i> Edit Post </a>
                    </li>
                    <li role="presentation"><a role="menuitem" class="dostums-post-delete" id="<?php echo $post->id; ?>" tabindex="-1" href="#"><i class="fa fa-trash"></i> Delete Post </a>
                    </li>
                    <!--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Hide from
                        Timeline</a></li>-->
                    <!--/*<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Add Location</a>
                    </li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Change Date</a>
                    </li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Turn off
                        notifications</a></li>
                    <li role="presentation" class="divider"></li>
                    */-->

                </ul>
            </div>

            <div class="panel-heading" id="<?php echo $new_post_head_id; ?>">
                <script type="text/javascript">
                    $(document).ready(function () {
                        load_post = {'st': 1, 'post_id':<?php echo $post->id; ?>};
                        $.post('lib/imageload.php', load_post, function (datapost) {
                            if (datapost != 0)
                            {
                                var datacl = jQuery.parseJSON(datapost);
                                var user_id = datacl.user_id;
                                var name = datacl.name;
                                var thumb = datacl.thumb;
                                var thumbbig = datacl.thumbbig;

                                var to_user_id = datacl.to_user_id;



                                if (to_user_id == 0)
                                {
                                    var sharedhtmlname = "Shared publicly";
                                    var sharedhtml = "Posted";
                                }
                                else
                                {

                                    var to_name = datacl.to_name;
                                    var thumb2 = datacl.thumb2;
                                    var thumbbig2 = datacl.thumbbig2;
                                    var sharedhtmlname = "Shared on <a href='profile.php?user_id=" + to_user_id + "' style='color:#000;'>" + to_name + "</a> Timeline";
                                    var sharedhtml = "Posted";
                                }

                                var datahtml = "<img class='img-circle pull-left' src='./profile/" + thumb + "' alt='" + thumb + "'><h3><a href='profile.php?user_id=" + user_id + "'>" + name + "</a> " + sharedhtmlname + "</h3><h5><span>" + sharedhtml + "</span> - <span><?php echo $extra->duration($post->post_time, date('Y-m-d H:i:s')); ?></span></h5>";

                                $('#<?php echo $new_post_head_id; ?>').html(datahtml);
                            }
                            else
                            {
                                window.location.refresh();
                            }
                        });

                    });
                </script>
            </div>
        <?php include('status/post.php'); ?>
            <div class="panel-bottom">
                <div class="panel-footer has-share-panel">
                    <div class="row">
                        <div class="col-sm-12">
            <?php include('status/postactionbar.php'); ?>
                        </div>
                    </div>
                </div>
                            <?php include('status/comment_list.php'); ?>
                            <?php include('status/comment.php'); ?>
            </div>
        </div>
                <?php
            //status one and 2 end
                $postbreak++;
        }
        
        if($postbreak==6)
        {
            break;
        }

    endforeach;
}
?>                        